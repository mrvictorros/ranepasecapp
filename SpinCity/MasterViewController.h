//
//  MasterViewController.h
//  SpinCity
//
//  Created by Росляков Виктор on 05.09.16.
//  Copyright © 2016 Росляков Виктор. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;


@end

