//
//  DetailViewController.h
//  SpinCity
//
//  Created by Росляков Виктор on 05.09.16.
//  Copyright © 2016 Росляков Виктор. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

