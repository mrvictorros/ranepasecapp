//
//  main.m
//  SpinCity
//
//  Created by Росляков Виктор on 05.09.16.
//  Copyright © 2016 Росляков Виктор. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
