//
//  Discount.m
//  RanepaApp
//
//  Created by Росляков Виктор on 02.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "Discount.h"

@implementation Discount

+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        
        [mapping mapKeyPath:@"TOTAL" toProperty:@"TOTAL" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"ALL" toProperty:@"ALL" withValueBlock:transformBlock];
        
    }];
}

@end
