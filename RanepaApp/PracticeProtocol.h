//
//  PracticeProtocol.h
//  RanepaApp
//
//  Created by Росляков Виктор on 22.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

@protocol PracticeProtocol

- (void)didCompletedPracticeFetch:(id)practiceArray;

- (void)didCompleteWithError:(NSError*)error;


@end
