//
//  DoubleViewCellObject.m
//  RanepaApp
//
//  Created by Росляков Виктор on 06.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "DoubleViewCellObject.h"

@implementation DoubleViewCellObject

+ (NSString*)identifierForReusableCell {
    return NSStringFromClass([DoubleViewTableViewCell class]);
}

@end
