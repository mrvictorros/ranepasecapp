//
//  RecordBook.m
//  RanepaApp
//
//  Created by Росляков Виктор on 17.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "RecordBook.h"

@implementation RecordBook

+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    id(^transformBlock1)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSNumber class]]) {
            return value;
        }
        return 0;
    };
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"Date" toProperty:@"Date" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Semestr" toProperty:@"Semestr" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Period" toProperty:@"Period" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Type" toProperty:@"Type" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Discipline" toProperty:@"Discipline" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Result" toProperty:@"Result" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_GodObuch" toProperty:@"UID_GodObuch" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Obuch" toProperty:@"UID_Obuch" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Semestr" toProperty:@"UID_Semestr" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_StudentWork" toProperty:@"UID_StudentWork" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Type" toProperty:@"UID_Type" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"WorkTitle" toProperty:@"WorkTitle" withValueBlock:transformBlock];
        
        [mapping mapKeyPath:@"Hours" toProperty:@"Hours" withValueBlock:transformBlock1];
        
        [mapping hasOne:[Obj_Tutor class] forKeyPath:@"Obj_Tutor"];
        
        
        }];
    }
        
@end
