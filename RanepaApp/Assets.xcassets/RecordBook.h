//
//  RecordBook.h
//  RanepaApp
//
//  Created by Росляков Виктор on 17.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"
#import "Obj_Tutor.h"


@interface RecordBook : NSObject <EKMappingProtocol>

@property(nonatomic, copy)NSString* Date;
@property(nonatomic, copy)NSString* Semestr;
@property(nonatomic, copy)NSString* Period;
@property(nonatomic, copy)NSString* Type;
@property(nonatomic, copy)NSString* Discipline;// строка UID дисциплины,
@property(nonatomic, copy)NSString* Result;// строка Результат сдачи,
@property(nonatomic, copy)NSString* UID_GodObuch;// строка UID года обучения,
@property(nonatomic, copy)NSString* UID_Obuch;// строка Идентификатор обучающегося,
@property(nonatomic, copy)NSString* UID_Semestr;// строка UID семестра,
@property(nonatomic, copy)NSString* UID_StudentWork;// строка Идентификатор курсовой или выпускной квалификационной работы,
@property(nonatomic, copy)NSString* UID_Type;// строка UID испытания (экзамен, зачёт, дифференцированный зачёт, курсовая работа, государственный экзамен, выпускная квалификационная работа),
@property(nonatomic, copy)NSString* WorkTitle;// строка Название курсовой или выпускной квалификационной работы,

@property(nonatomic, copy)NSNumber* Hours;// число Академических часов обучения,

@property (nonatomic, readwrite) BOOL Passed;// булево, считается успешно сданным,
@property (nonatomic, readwrite) BOOL Retake;// булево, является пересдачей,

@property (nonatomic, strong) Obj_Tutor *Obj_Tutor;

+(EKObjectMapping *)objectMapping;


@end
