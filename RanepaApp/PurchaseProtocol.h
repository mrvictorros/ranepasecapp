//
//  PurchaseProtocol.h
//  RanepaApp
//
//  Created by Росляков Виктор on 02.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

@protocol PurchaseProtocol

- (void)didCompletedPurchaseFetch:(id)purchaseData;

- (void) didCompletedPostPurchase;

- (void)didCompleteWithError:(NSError*)error;


@end
