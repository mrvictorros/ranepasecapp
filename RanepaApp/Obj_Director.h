//
//  Obj_Director.h
//  RanepaApp
//
//  Created by Росляков Виктор on 22.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"


@interface Obj_Director : NSObject <EKMappingProtocol>

@property(nonatomic, strong)NSString* Email;
@property(nonatomic, strong)NSString* FIO;
@property(nonatomic, strong)NSString* Phone;
@property(nonatomic, strong)NSString* Post;

+(EKObjectMapping *)objectMapping;

@end
