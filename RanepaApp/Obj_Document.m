//
//  Obj_Document.m
//  RanepaApp
//
//  Created by Росляков Виктор on 23.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "Obj_Document.h"


@implementation Obj_Document

+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    id(^transformBlock1)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSNumber class]]) {
            return value;
        }
        return 0;
    };
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"DataVidachi" toProperty:@"DataVidachi" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"KemVydan" toProperty:@"KemVydan" withValueBlock:transformBlock];
        
        [mapping mapKeyPath:@"Nomer" toProperty:@"Nomer" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Seriya" toProperty:@"Seriya" withValueBlock:transformBlock];
        
        [mapping hasOne:[Obj_File class] forKeyPath:@"Obj_File"];
        
        
    }];
}

@end
