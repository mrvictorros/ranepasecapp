//
//  FaqFetch.h
//  RanepaApp
//
//  Created by Росляков Виктор on 29.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FaqProtocol.h"

@interface FaqFetch : NSObject

- (void)getFaqData;

-(id)initWithDelegate:(id)delegate;

@property(nonatomic, weak)id <FaqProtocol> delegate;

@end
