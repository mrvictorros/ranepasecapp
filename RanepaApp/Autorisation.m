//
//  Autorisation.m
//  RanepaApp
//
//  Created by Росляков Виктор on 14.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "Autorisation.h"
#include <stdlib.h>
#import <CommonCrypto/CommonDigest.h>
#import "ProfileHTTPCommunication.h"
#import <MagicalRecord/MagicalRecord.h>
#import "Reg.h"


@interface Autorisation()

@property(nonatomic, copy) NSDictionary *keyBase;

@end


@implementation Autorisation

-(id)initWithDelegate:(id)delegate{
    self = [super init];
    if(self){
        _delegate = delegate;
    }
    return self;
}

-(void)autorisation{
    
    self.appCode = [NSString stringWithFormat:@"%@-%@",@"LK-iOS",[[UIDevice currentDevice] identifierForVendor].UUIDString];

    NSString *md5AppCode =[self md5:self.appCode];
    NSInteger legh=md5AppCode.length - 16;
    NSString *sim =[md5AppCode substringFromIndex:legh];
    self.simCode = sim;
//Алгоритм. Подготовительный этап. Происходит инициализация переменных и функций, не зависящих от входных параметров.
    NSInteger apocalypse= 1356078072;//seconds 1356078072=2012-12-21T12:21:12 . Постоянная величина для отсчёта времени.
    NSInteger priority=arc4random_uniform(7)+1; // случайная величина, влияющая на выбор пары ключей.
//    NSLocale* currentLocale = [NSLocale currentLocale];
//    [[NSDate date] descriptionWithLocale:currentLocale];
    NSInteger moment = [[NSDate date] timeIntervalSince1970];//seconds 0=1970-01-01T00:00:00(+00:00) // вторая случайная величина.
    NSInteger repeater= priority+ moment%100; // расчёт случайного значения, необходимого для формирования цепочки ключей.
    NSInteger before=repeater/2; // вспомогательные переменныену
    NSInteger after=repeater-before;
    NSString *regid = [NSString stringWithFormat:@"%@%@",[_appCode lowercaseString],_simCode];
    NSString *access = [NSString stringWithFormat:@"%@%@%@",[self repeatTimes:_simCode:before],[_appCode uppercaseString],[self repeatTimes:_simCode:after]];
    NSString *dec = [NSString stringWithFormat: @"%ld", (NSInteger)(moment-apocalypse)];
    NSString *hex = [NSString stringWithFormat:@"%lX",(unsigned long)[dec integerValue]];
//Алгоритм. Этап формирования ключевого базиса, собирает основные значения.
    if(hex!=nil){
    _keyBase = [NSDictionary dictionaryWithObjectsAndKeys:
                _appCode,@"application", //код приложения
                [self md5:regid],@"regid", //регистрационный ключ, постоянное значение для приложения.
                [self sha1:access],@"access",//цепочка ключей. формируется с помощью случайных величин и констант приложения.
                [NSString stringWithFormat: @"%ld", (long)(priority-1)],@"priority",//переменная для определения выбора пары ключей.
                hex,@"pass",
                             nil//временной код от точки отсчёта.
                ];
    }
//Алгоритм. Этап получения сессионных ключей, основывается на значениях ключевого базиса.
    
//    NSDictionary tokarr=[NSDictionary dictionaryWithObjectsAndKeys:
//                         ['public'=>md5(substr($keyBase['access'],($keyBase['priority']-1)*4,2)+$keyBase['pass']),'private'=>md5(substr($keyBase['access'],($keyBase['priority']-1)*4+2,3)+$keyBase['pass'])];
//Алгоритм. Получение строки запроса. Данное значение должно быть передано в функцию авторизации центрального API
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:_keyBase options:NSJSONReadingMutableContainers error:&error];
    NSString* newStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    newStr= [newStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    NSString* coder = [self coderFunction:newStr];
    NSString *applicationKey = [self urlEncode:coder];
    // 1
   //NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"https://10.100.51.128:9111/auth?key=",applicationKey]];
    NSString *stringUrl = [NSString stringWithFormat:@"https://lk-api.ranepa.ru/auth?key=%@",applicationKey];
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                          delegate:self
                                                     delegateQueue:Nil];
    
    // 2
    __weak typeof(self)weakSelf = self;
    NSURLSessionDataTask *downloadTask = [session dataTaskWithURL:url
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              __strong typeof(self)strongSelf = weakSelf;
                                              // десериализуем полученную информацию
                            NSArray *datat = [NSJSONSerialization JSONObjectWithData:data
                                                                              options:0
                                                                                error:&error];
                            NSLog(@"%@",datat);
                            _returnCode= datat[0][@"original"][@"code"];
                            NSString *status = datat[0][@"original"][@"status"];
                            long codeTest = 200;
                            if ([_returnCode longLongValue] == codeTest) {
                                [strongSelf generateKeys];
                            }
                             dispatch_async(dispatch_get_main_queue(), ^{
                            [strongSelf.delegate didCompletedAutorization];
                             });
    }];
    
    // 3
    [downloadTask resume];
//    if(self.publicKey == nil){
//        return FALSE;
//    }
//    else{
//        return TRUE;
//    }
    
};
-(void)generateKeys{
    self.publicKey = [self md5:[NSString stringWithFormat:@"%@%@",[[_keyBase objectForKey:@"access"] substringWithRange:NSMakeRange((([[_keyBase objectForKey:@"priority"] integerValue]-1)*4),2)],[_keyBase objectForKey:@"pass"]]];
    self.privateKey = [self md5:[NSString stringWithFormat:@"%@%@",[[_keyBase objectForKey:@"access"] substringWithRange:NSMakeRange((([[_keyBase objectForKey:@"priority"] integerValue]-1)*4+2),3)],[_keyBase objectForKey:@"pass"]]];
    
    if (!self.reg) {
        self.reg = [Reg MR_createEntity];
        
    }
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:self.publicKey forKey:@"publicKey"];
    self.reg.appcode = _appCode;
    self.reg.simcode = _simCode;
    self.reg.publiccode = self.publicKey;
    self.reg.privatecode = self.privateKey;
    
    
}
- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
        if([challenge.protectionSpace.host isEqualToString:@"lk-api.ranepa.ru"]){
            NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
        }
    }
}
-(NSString *) urlEncode:(NSString*) stringToEncode {
        NSString *result = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                                 NULL,
                                                                                                 (CFStringRef)stringToEncode,
                                                                                                 NULL,
                                                                                                 (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                                                                 kCFStringEncodingUTF8
                                                                                                 ));
        result = [result stringByReplacingOccurrencesOfString:@"%20" withString:@"+"];
        return result;
    
}
-(NSString *)coderFunction:(NSString*) input{
    NSString *retstr= @"";
    for (NSUInteger i = 0; i<[input length]; i++) {
        int asciiCode =[input characterAtIndex:i];
        retstr = [NSString stringWithFormat:@"%@%@",retstr,[NSString stringWithFormat:@"%c",(asciiCode+2)]];
    }
    return retstr;
}
- (NSString *) md5:(NSString *) input
    {
        const char *cStr = [input UTF8String];
        unsigned char digest[CC_MD5_DIGEST_LENGTH];
        CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
        
        NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
        
        for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
            [output appendFormat:@"%02x", digest[i]];
        
        return  output;
    }
- (NSString *)sha1:(NSString *)str {
                     const char *cStr = [str UTF8String];
                     unsigned char result[CC_SHA1_DIGEST_LENGTH];
                     CC_SHA1(cStr, strlen(cStr), result);
                     NSString *s = [NSString  stringWithFormat:
                                    @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                                    result[0], result[1], result[2], result[3], result[4],
                                    result[5], result[6], result[7],
                                    result[8], result[9], result[10], result[11], result[12],
                                    result[13], result[14], result[15],
                                    result[16], result[17], result[18], result[19]
                                    ];
                     
                     return s;
}
- (NSString *)repeatTimes:(NSString*)string:(NSUInteger)times {
                             return [@"" stringByPaddingToLength:times * [string length] withString:string startingAtIndex:0];
                         }
@end
