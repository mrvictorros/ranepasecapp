//
//  Schedule.m
//  RanepaApp
//
//  Created by Росляков Виктор on 04.08.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "Schedule.h"

@implementation Schedule

+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    id(^transformBlock1)(NSString *key, id value) = ^id(NSString *key, id value) {
        NSString *value2  = [[value allValues] objectAtIndex:0];
        if ([value2 isKindOfClass:[NSString class]]) {
            return value2;
        }
        return @"";
    };
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[
                                          @"groups",
                                          @"UID_type",
                                          @"comment"]];
        [mapping mapKeyPath:@"startDate" toProperty:@"startDate" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"status" toProperty:@"status" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"endDate" toProperty:@"endDate" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_room" toProperty:@"UID_room" withValueBlock:transformBlock1];
        [mapping mapKeyPath:@"UID_disc" toProperty:@"UID_disc" withValueBlock:transformBlock1];
        [mapping mapKeyPath:@"UID_lecturer" toProperty:@"UID_lecturer" withValueBlock:transformBlock1];
        [mapping mapKeyPath:@"UID_type" toProperty:@"UID_type" withValueBlock:transformBlock1];
    }];
    
}

@end
