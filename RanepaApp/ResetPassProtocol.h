//
//  ResetPassProtocol.h
//  RanepaApp
//
//  Created by Росляков Виктор on 29.06.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

@protocol ResetPassProtocol

- (void)didCompletedResetPassFetch:(id)resetPass;

- (void)didCompleteWithError:(NSError*)error;


@end
