//
//  ScheduleFetch.h
//  RanepaApp
//
//  Created by Росляков Виктор on 04.08.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ScheduleProtocol.h"


@interface ScheduleFetch : NSObject

-(void)getScheduleData:(int)offset:(NSString*)date;

-(id)initWithDelegate:(id)delegate;

@property(nonatomic, weak)id <ScheduleProtocol> delegate;

@end
