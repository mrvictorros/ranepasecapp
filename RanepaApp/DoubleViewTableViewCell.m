//
//  DoubleViewTableViewCell.m
//  RanepaApp
//
//  Created by Росляков Виктор on 06.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "DoubleViewTableViewCell.h"
#import "DoubleViewCellObject.h"

static const CGFloat kCellHeight = 66.0f;

@implementation DoubleViewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellWithObject:(DoubleViewCellObject *)object {
    
    _titleLeftLabel.text = object.titleLeft;
    _subTitleLeftLabel.text = object.subTitleLeft;
    _titleRightLabel.text = object.titleRight;
    _subTitleRightLabel.text = object.subTitleRight;
    
}

+ (CGFloat)heightForObject:(id)object
               atIndexPath:(NSIndexPath *)indexPath
                 tableView:(UITableView *)tableView {
    return kCellHeight;
}

@end
