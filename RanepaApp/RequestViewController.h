//
//  RequestViewController.h
//  RanepaApp
//
//  Created by Росляков Виктор on 17.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *requestTableView;

@end
