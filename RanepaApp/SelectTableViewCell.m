//
//  SelectTableViewCell.m
//  RanepaApp
//
//  Created by Росляков Виктор on 31.01.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "SelectTableViewCell.h"
#import "NotificationConstants.h"

static const CGFloat kCellHeight = 66.0f;
static const CGFloat HeightDataPickerCell = 216.0f;

@interface SelectTableViewCell ()

@property (nonatomic,strong) SelectCellObject *cellObject;

@end

@implementation SelectTableViewCell

- (void)updateCellWithObject:(SelectCellObject *)object {
    _cellObject =object;
    _titleSelectCell.text = [object.titleArray objectAtIndex:object.selectedIndex];
    _subTitleSelectCell.text = object.subTitleSelectEdit;
}


- (IBAction)didTapExpandCell:(id)sender {
    self.cellObject.isExpanded = !self.cellObject.isExpanded;
    [[NSNotificationCenter defaultCenter] postNotificationName:kSelectTableViewCellExpandNotification
                                                        object:nil];
}

+ (CGFloat)heightForObject:(SelectCellObject *)object
               atIndexPath:(NSIndexPath *)indexPath
                 tableView:(UITableView *)tableView {
    if (object.isExpanded) {
        return kCellHeight + HeightDataPickerCell;
    }
    return kCellHeight;
}

#pragma mark - Data Picker Data Source

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.cellObject.titleArray count];
}

#pragma mark - Data Picker Delegate

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.cellObject.titleArray[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.cellObject.selectedIndex = row;
    self.titleSelectCell.text = self.cellObject.titleArray[row];
}

@end
