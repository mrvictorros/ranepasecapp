//
//  SelectTableViewCell.h
//  RanepaApp
//
//  Created by Росляков Виктор on 31.01.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectCellObject.h"
#import "TableViewCellProtocol.h"

@interface SelectTableViewCell : UITableViewCell <TableViewCellProtocol, UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic) IBOutlet UILabel *titleSelectCell;
@property (strong, nonatomic) IBOutlet UILabel *subTitleSelectCell;
@property (strong, nonatomic) IBOutlet UIImageView *selectedImageSelectCell;
@property (strong, nonatomic) IBOutlet UIPickerView *dataPicker;

@end
