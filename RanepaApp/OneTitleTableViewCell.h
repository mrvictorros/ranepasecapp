//
//  OneTitleTableViewCell.h
//  RanepaApp
//
//  Created by Росляков Виктор on 06.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewCellProtocol.h"

@interface OneTitleTableViewCell : UITableViewCell <TableViewCellProtocol>

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
