//
//  EditTableViewCell.m
//  RanepaApp
//
//  Created by Росляков Виктор on 31.01.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "EditTableViewCell.h"

static const CGFloat kCellHeight = 66.0f;

@interface EditTableViewCell()

@property (strong, nonatomic) EditCellObject *object;
@property (nonatomic, assign) CGPoint lastTablePosition;



@end

@implementation EditTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self addGestureRecognizer:tap];
}
-(void)dismissKeyboard
{
    [self.textFieldEditCell resignFirstResponder];
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.superview.superview.frame;
        f.origin.y = 0.0f+kCellHeight;
        self.superview.superview.frame = f;
    }];
}
- (void)updateCellWithObject:(EditCellObject *)object {
    self.object = object;
}
- (IBAction)editDidEnd:(id)sender {
    
}
- (IBAction)editDidChanged:(id)sender {
    self.object.textFieldEdit = self.textFieldEditCell.text;
}

+ (CGFloat)heightForObject:(id)object
               atIndexPath:(NSIndexPath *)indexPath
                 tableView:(UITableView *)tableView {
    
    return kCellHeight;
}

//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
//    // TODO: Не самое лучшее решение, в будущем поправить
//    UITableView *tableView = (UITableView *)self.superview.superview;
//    CGPoint scrollPosition = CGPointMake(0, textField.frame.origin.y + kCellHeight);
//    // совсем хуевое решение, переделать
//    self.lastTablePosition = tableView.contentOffset;
//    [tableView setContentOffset:scrollPosition
//                       animated:YES];
//    return YES;
//}
//
//- (void)textFieldDidEndEditing:(UITextField *)textField {
//    [textField resignFirstResponder];
//    UITableView *tableView = (UITableView *)self.superview.superview;
//    [tableView setContentOffset:self.lastTablePosition
//                       animated:YES];
//}
//- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    [textField resignFirstResponder];
//    UITableView *tableView = (UITableView *)self.superview.superview;
//    [tableView setContentOffset:self.lastTablePosition
//                       animated:YES];
//    return YES;
//}
#pragma mark - keyboard movements
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    return YES;
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.superview.superview.frame;
        f.origin.y = -keyboardSize.height ;
        self.superview.superview.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    
}
@end
