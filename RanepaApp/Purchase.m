//
//  Purchase.m
//  RanepaApp
//
//  Created by Росляков Виктор on 02.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "Purchase.h"

@implementation Purchase

+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    id(^transformBlock1)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSNumber class]]) {
            return value;
        }
        return 0;
    };
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        
        [mapping mapKeyPath:@"CONTRACT" toProperty:@"CONTRACT" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"FirstYear" toProperty:@"FirstYear" withValueBlock:transformBlock];
        
        [mapping mapKeyPath:@"NEEDPAY" toProperty:@"NEEDPAY" withValueBlock:transformBlock1];
        [mapping mapKeyPath:@"PENYA" toProperty:@"PENYA" withValueBlock:transformBlock1];
        [mapping mapKeyPath:@"PAYED" toProperty:@"PAYED" withValueBlock:transformBlock1];
        [mapping mapKeyPath:@"COST" toProperty:@"COST" withValueBlock:transformBlock1];
        
        [mapping hasMany:[Periods class] forKeyPath:@"PERIODS"];
        [mapping hasMany:[Payments class] forKeyPath:@"PAYMENTS"];
        [mapping hasMany:[Payer class] forKeyPath:@"PAYER"];

        [mapping hasOne:[Discount class] forKeyPath:@"DISCOUNT"];

        
        
    }];
}

@end
