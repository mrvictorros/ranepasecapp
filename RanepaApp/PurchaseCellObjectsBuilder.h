//
//  PurchaseCellObjectsBuilder.h
//  RanepaApp
//
//  Created by Росляков Виктор on 03.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Purchase.h"

@interface PurchaseCellObjectsBuilder : NSObject

+ (NSArray *) buildCellObjectsWithPurchase:(Purchase *) purchase;

@end
