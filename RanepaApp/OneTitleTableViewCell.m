//
//  OneTitleTableViewCell.m
//  RanepaApp
//
//  Created by Росляков Виктор on 06.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "OneTitleTableViewCell.h"
#import "OneTitleCellObject.h"

static const CGFloat kCellHeight = 90.0f;

@implementation OneTitleTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellWithObject:(OneTitleCellObject *)object {
    
    _titleLabel.text = object.titleName;
    
}

+ (CGFloat)heightForObject:(id)object
               atIndexPath:(NSIndexPath *)indexPath
                 tableView:(UITableView *)tableView {
    return kCellHeight;
}

@end
