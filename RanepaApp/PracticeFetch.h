//
//  PracticeFetch.h
//  RanepaApp
//
//  Created by Росляков Виктор on 22.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PracticeProtocol.h"

@interface PracticeFetch : NSObject

- (void)getPracticeData;

-(id)initWithDelegate:(id)delegate;

@property(nonatomic, weak)id <PracticeProtocol> delegate;

@end
