//
//  FaqProtocol.h
//  RanepaApp
//
//  Created by Росляков Виктор on 29.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//


@protocol FaqProtocol


- (void)didCompletedFaqFetch:(id)arrayOfArray;

- (void)didCompleteWithError:(NSError*)error;


@end