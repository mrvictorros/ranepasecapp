//
//  ResetPass.h
//  RanepaApp
//
//  Created by Росляков Виктор on 29.06.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"

@interface ResetPass : NSObject

@property(strong,nonatomic)NSString* Status;
@property(strong,nonatomic)NSString* Message;

+(EKObjectMapping *)objectMapping;

@end
