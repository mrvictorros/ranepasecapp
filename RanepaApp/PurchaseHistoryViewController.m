//
//  PurchaseHistoryViewController.m
//  RanepaApp
//
//  Created by Росляков Виктор on 06.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "PurchaseHistoryViewController.h"
#import "SWRevealViewController.h"
#import "PurchaseFetch.h"
#import "PurchaseHistoryCellObjectsBuilder.h"
#import "PurchaseViewController.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface PurchaseHistoryViewController ()

@property (strong, nonatomic) Purchase *purchase;

@end

@implementation PurchaseHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    SWRevealViewController *revealViewController = self.revealViewController;
    [self.payButton setEnabled:NO];
    [self.payButton setTintColor: [UIColor clearColor]];
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    UIFont *font = [UIFont fontWithName:@"helvetica-light" size:15.0];
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    UIColorFromRGB(0x6D1416),NSForegroundColorAttributeName,
                                    font,NSFontAttributeName,nil];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.title = @"Оплата обучения";
    PurchaseFetch *pf = [[PurchaseFetch alloc]initWithDelegate:self];
    [pf getPurchaseData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)didCompleteWithError:(NSError*)error{
    //    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Ошибка"
    //                                                                   message:@"Проблемы при загрузке данных."
    //                                                            preferredStyle:UIAlertControllerStyleAlert];
    //
    //    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
    //                                                          handler:^(UIAlertAction * action) {}];
    //
    //    [alert addAction:defaultAction];
    //    [self presentViewController:alert animated:YES completion:nil];
    int yPosition = 10;
    UILabel *label = [[UILabel alloc] initWithFrame: CGRectMake(0, yPosition, self.view.frame.size.width, 0)];
    [label setText: @"Отстутствуют данные об оплате обучения"];
    [label setBackgroundColor: [UIColor clearColor]];
    [label setNumberOfLines: 0];
    [label sizeToFit];
    [label setCenter: CGPointMake(self.view.center.x, self.view.center.y)];
    [[self view] addSubview:label];
}
- (void)didCompletedPurchaseFetch:(id)purchaseData{
    _purchase = purchaseData;
    if((!_purchase.ENABLE)||(self.purchase == nil)){ //check for show/unshow nextscreenbutton
        [self.payButton setEnabled:NO];
        [self.payButton setTintColor: [UIColor clearColor]];
    }
    else{
        [self.payButton setEnabled:YES];
        [self.payButton setTintColor: UIColorFromRGB(0x6D1416)];
    }
    NSArray *cellObjects = [PurchaseHistoryCellObjectsBuilder buildCellObjectsWithPurchase:_purchase];
    self.tableDelegate = [[PaymentTableDelegate alloc] init];
    self.purchaseHistoryTableView.dataSource = self.tableDelegate;
    self.purchaseHistoryTableView.delegate = self.tableDelegate;
    self.tableDelegate.cellObjects = cellObjects;
    [self.purchaseHistoryTableView reloadData];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"payFormOperSeque"]){
        PurchaseViewController *controller = (PurchaseViewController *)segue.destinationViewController;
        controller.purchase = _purchase;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
