//
//  PurchaseFetch.m
//  RanepaApp
//
//  Created by Росляков Виктор on 01.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "PurchaseFetch.h"
#import "EasyMapping.h"
#import "Purchase.h"

@implementation PurchaseFetch

-(id)initWithDelegate:(id)delegate{
    self = [super init];
    if(self){
        _delegate = delegate;
    }
    return self;
}

- (void)postPurchase:(NSString *)urlString{
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                          delegate:self
                                                     delegateQueue:Nil];
    
    // 2
    __weak typeof(self)weakSelf = self;
    NSURLSessionDataTask *downloadTask = [session
                                          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              __strong typeof(self)strongSelf = weakSelf;
                                              if (data==nil) {
                                                  NSError *error = [NSError errorWithDomain:@"ServerApiError"
                                                                                       code:nil
                                                                                   userInfo:nil];
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [strongSelf.delegate didCompleteWithError:error];
                                                  });
                                                  return;
                                                  
                                              }
                                              NSArray *datat = [NSJSONSerialization JSONObjectWithData:data
                                                                                               options:0
                                                                                                 error:&error];
                                              
                                              NSDictionary *jsonError = datat[0][@"body"][@"ERROR"];
                                              if([jsonError count] ){
                                                  NSError *error = [NSError errorWithDomain:@"ServerApiError"
                                                                                       code:[jsonError objectForKey:@"CODE"]
                                                                                   userInfo:jsonError];
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [strongSelf.delegate didCompleteWithError:error];
                                                  });
                                                  return;
                                              }
                                   
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [strongSelf.delegate didCompletedPostPurchase];
                                                  
                                              });
                                              
                                          }];
    //                                                                                            for(NSInteger i=1;i<json.count+1;i++){
    //                                                                                                NSString *string = [NSString stringWithFormat:@"%zd", i];
    //                                                                                                NSArray *carsArray = [EKMapper arrayOfObjectsFromExternalRepresentation:json[string]
    //                                                                                                withMapping:[AcademicPlan objectMapping]];
    //                                                                                        [finalArray addObject:carsArray];
    
    
    
    
    
    // 3
    [downloadTask resume];
}
- (void)getPurchaseData{
    
    // 1
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults objectForKey:@"url"];
    NSString *dataUrl = [NSString stringWithFormat:@"%@name=stud-pay&%@",[userDefaults objectForKey:@"url"],[userDefaults objectForKey:@"secondUrl"]];
    NSURL *url = [NSURL URLWithString:dataUrl];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                          delegate:self
                                                     delegateQueue:Nil];
    
    // 2
    __weak typeof(self)weakSelf = self;
    NSURLSessionDataTask *downloadTask = [session
                                          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              __strong typeof(self)strongSelf = weakSelf;
                                              if (data==nil) {
                                                  NSError *error = [NSError errorWithDomain:@"ServerApiError"
                                                                                       code:nil
                                                                                   userInfo:nil];
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [strongSelf.delegate didCompleteWithError:error];
                                                  });
                                                  return;
                                                  
                                              }
                                              NSArray *datat = [NSJSONSerialization JSONObjectWithData:data
                                                                                               options:0
                                                                                                 error:&error];
                                              NSMutableArray *periodsArray = [NSMutableArray new] ;
                                              NSDictionary *jsonError = nil;
                                              if([datat[0][@"body"][@"ERROR"] isKindOfClass:[NSDictionary class]])
                                                  jsonError = datat[0][@"body"][@"ERROR"];
                                              if([jsonError count] ){
                                                  NSError *error = [NSError errorWithDomain:@"ServerApiError"
                                                                                       code:[jsonError objectForKey:@"CODE"]
                                                                                   userInfo:jsonError];
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [strongSelf.delegate didCompleteWithError:error];
                                                  });
                                                  return;
                                              }
                                              NSDictionary *json= datat[0][@"body"][@"DATA"];

                                              Purchase *purchase = [EKMapper objectFromExternalRepresentation:json
                                                                                                          withMapping:[Purchase objectMapping]];
                                              purchase.URL = [datat[0][@"body"][@"FORM"] objectForKey:@"URL"];
                                              purchase.ENABLE = [[datat[0][@"body"][@"FORM"] objectForKey:@"ENABLE"]boolValue];
                                              purchase.DepCode =[datat[0][@"body"][@"FORM"] objectForKey:@"DepCode"];
                                              purchase.truePeriods = [datat[0][@"body"][@"FORM"] objectForKey:@"Periods"];
//                                              NSArray *keysPeriods = [json1 allKeys];
//                                              for (int i =0; i<[keysPeriods count]; i++) {
//                                                  [json1 objectForKey:keysPeriods[i]];
//                                                  [periodsArray addObject:[EKMapper objectFromExternalRepresentation:[json1 objectForKey:keysPeriods[i]] withMapping:[Periodss objectMapping]]];
//                                                  
//                                              }
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [strongSelf.delegate didCompletedPurchaseFetch :purchase];
                                                  
                                              });
                                              
                                          }];
    //                                                                                            for(NSInteger i=1;i<json.count+1;i++){
    //                                                                                                NSString *string = [NSString stringWithFormat:@"%zd", i];
    //                                                                                                NSArray *carsArray = [EKMapper arrayOfObjectsFromExternalRepresentation:json[string]
    //                                                                                                withMapping:[AcademicPlan objectMapping]];
    //                                                                                        [finalArray addObject:carsArray];
    
    
    
    
    
    // 3
    [downloadTask resume];
    
}


- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
        if([challenge.protectionSpace.host isEqualToString:@"lk-api.ranepa.ru"]){
            NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
        }
    }
}


@end
