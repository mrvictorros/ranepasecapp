//
//  AchievementsFetch.h
//  RanepaApp
//
//  Created by Росляков Виктор on 22.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AchievementsProtocol.h"

@interface AchievementsFetch : NSObject

- (void)getAchievementsData;

-(id)initWithDelegate:(id)delegate;

@property(nonatomic, weak)id <AchievementsProtocol> delegate;

@end
