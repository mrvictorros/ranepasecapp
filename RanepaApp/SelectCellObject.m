//
//  SelectCellObject.m
//  RanepaApp
//
//  Created by Росляков Виктор on 31.01.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "SelectCellObject.h"
#import "SelectTableViewCell.h"

@implementation SelectCellObject

-(NSString *) typeOfCell{
    return @"SelectCellObject";
}

+ (NSString*)identifierForReusableCell {
    return @"SelectTableViewCell";
}

@end
