//
//  HelpFetch.h
//  RanepaApp
//
//  Created by Росляков Виктор on 23.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HelpProtocol.h"

@interface HelpFetch : NSObject

- (void)getHelpData;

-(id)initWithDelegate:(id)delegate;

@property(nonatomic, weak)id <HelpProtocol> delegate;

@end
