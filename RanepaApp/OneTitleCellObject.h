//
//  OneTitleCellObject.h
//  RanepaApp
//
//  Created by Росляков Виктор on 06.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OneTitleTableViewCell.h"
#import "TableViewCellObjectProtocol.h"

@interface OneTitleCellObject : NSObject <TableViewCellObjectProtocol>

@property(nonatomic, strong)NSString *titleName;

+ (NSString *)identifierForReusableCell;

@end
