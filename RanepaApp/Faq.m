//
//  Faq.m
//  RanepaApp
//
//  Created by Росляков Виктор on 29.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "Faq.h"

@implementation Faq

+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    id(^transformBlock1)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSNumber class]]) {
            return value;
        }
        return 0;
    };
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"NAME" toProperty:@"NAME" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"PREVIEW_TEXT" toProperty:@"PREVIEW_TEXT" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"DETAIL_TEXT" toProperty:@"DETAIL_TEXT" withValueBlock:transformBlock];
        
        [mapping mapKeyPath:@"ID" toProperty:@"ID" withValueBlock:transformBlock1];
        [mapping mapKeyPath:@"IBLOCK_ID" toProperty:@"IBLOCK_ID" withValueBlock:transformBlock1];
        [mapping mapKeyPath:@"IBLOCK_SECTION_ID" toProperty:@"IBLOCK_SECTION_ID" withValueBlock:transformBlock1];
        

        
    }];
}

@end
