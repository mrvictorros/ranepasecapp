//
//  ResetPass.m
//  RanepaApp
//
//  Created by Росляков Виктор on 29.06.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "ResetPass.h"

@implementation ResetPass
+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };

    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"Status" toProperty:@"Status" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Message" toProperty:@"Message" withValueBlock:transformBlock];
    }];
}

@end
