//
//  PracticeDataService.m
//  RanepaApp
//
//  Created by Росляков Виктор on 10.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "PracticeDataService.h"
#import "Practice.h"
@interface PracticeDataService ()




@end

@implementation PracticeDataService
-(id) init {
    self = [super init];
    
    if(self) {
        _practiceArray = [[NSMutableArray alloc] init];
        [self initializePractice];
        return self;
    }
    return nil;
}

-(void)addPractice:(NSString *)positionPractice companyPractice:(NSString *)companyPractice datePractice:(NSString *)datePractice
             placePractice:(NSString *)placePractice dutiesPractice:(NSString *)dutiesPractice
            headCompanyPractice:(NSString *)headCompanyPractice headRanepaPractice:(NSString *)headRanepaPractice
            urlDocOne:(NSString *)urlDocOne urlDocTwo:(NSString *)urlDocTwo urlDocThree:(NSString *)urlDocThree   {
    Practice *practice = [[Practice alloc] init];
    practice.positionPractice = positionPractice;
    practice.companyPractice = companyPractice;
    practice.datePractice = datePractice;
    practice.placePractice = placePractice;
    practice.dutiesPractice = dutiesPractice;
    practice.headCompanyPractice = headCompanyPractice;
    practice.headRanepaPractice = headRanepaPractice;
    practice.urlDocOne = urlDocOne;
    practice.urlDocTwo = urlDocTwo;
    practice.urlDocThree = urlDocThree;
    
    [_practiceArray addObject:practice];
}
-(void)initializePractice{
    [self addPractice:@"Практикант-Консультант" companyPractice:@"АО Сбербанк" datePractice:@"Июль 2010(1 месяц)" placePractice:@"г. Москва, Россия" dutiesPractice:@"Консультация физических лиц,помощь при работе с терминалами" headCompanyPractice:@"Петров Егор Александрович" headRanepaPractice:@"Ранепов Игорь Федорович" urlDocOne:@"http://www.yandex.ru" urlDocTwo:@"http://www.yandex.ru" urlDocThree:@"http://www.yandex.ru"];
    [self addPractice:@"Экономист-стажер" companyPractice:@"АО ИнвестЭкономикс" datePractice:@"Июнь 2014(3 месяц)" placePractice:@"г. Москва, Россия" dutiesPractice:@"Консультация физических лиц,помощь при работе с терминалами" headCompanyPractice:@"Петров Егор Александрович" headRanepaPractice:@"Ранепов Игорь Федорович" urlDocOne:@"http://www.yandex.ru" urlDocTwo:@"http://www.yandex.ru" urlDocThree:@"http://www.yandex.ru"];
    
}
-(NSUInteger)practiceCount {
    return [self.practiceArray count];
}

-(Practice *)practiceAtIndex:(NSUInteger)index {
    return [self.practiceArray objectAtIndex:index];
}


@end
