//
//  ScheduleViewController.h
//  RanepaApp
//
//  Created by Росляков Виктор on 26.07.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSCalendar.h"
#import "ScheduleFetch.h"

@interface ScheduleViewController : UIViewController  <FSCalendarDataSource, FSCalendarDelegate, ScheduleProtocol,UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet FSCalendar *calendar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;

@property (weak, nonatomic)IBOutlet UITableView *scheduleTableView;

+ (BOOL) dateCheck:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate;

@end
