//
//  ResetPassFetch.m
//  RanepaApp
//
//  Created by Росляков Виктор on 29.06.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "ResetPassFetch.h"
#import "ResetPass.h"

@implementation ResetPassFetch

-(id)initWithDelegate:(id)delegate{
    self = [super init];
    if(self){
        _delegate = delegate;
    }
    return self;
}

- (void)doReset:(NSString *)login{
NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
[userDefaults objectForKey:@"url"];
NSString *dataUrl = [NSString stringWithFormat:@"%@user=%@",@"https://lk-api.ranepa.ru/patchPass?",login];
NSURL *url = [NSURL URLWithString:dataUrl];
NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                      delegate:self
                                                 delegateQueue:Nil];

// 2
__weak typeof(self)weakSelf = self;
NSURLSessionDataTask *downloadTask = [session dataTaskWithURL:url
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                __strong typeof(self)strongSelf = weakSelf;
                                                if (data==nil) {
                                                    NSError *error = [NSError errorWithDomain:@"ServerApiError"
                                                                                         code:nil
                                                                                     userInfo:nil];
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        [strongSelf.delegate didCompleteWithError:error];
                                                    });
                                                    return;
                                                    
                                                }
                                                NSArray *datat = [NSJSONSerialization JSONObjectWithData:data
                                                                                                 options:0
                                                                                                   error:&error];
                                                NSDictionary *jsonError = datat[0][@"body"][@"ERROR"];
                                                if([jsonError count] ){
                                                    NSError *error = [NSError errorWithDomain:@"ServerApiError"
                                                                                         code:[jsonError objectForKey:@"CODE"]
                                                                                     userInfo:jsonError];
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        [strongSelf.delegate didCompleteWithError:error];
                                                    });
                                                    return;
                                                }
                                                
                                                
                                                
                                                NSArray *arrayData= datat[0][@"original"];
                                                ResetPass *resetPass = [EKMapper objectFromExternalRepresentation:arrayData
                                                                                                             withMapping:[ResetPass objectMapping]];
                                                
                                                
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                    
                                                    [strongSelf.delegate didCompletedResetPassFetch:resetPass];
                                                });
                                                
                                            }];

// 3
[downloadTask resume];

}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
        if([challenge.protectionSpace.host isEqualToString:@"lk-api.ranepa.ru"]){
            NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
        }
    }
}
@end
