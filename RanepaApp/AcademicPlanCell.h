//
//  AcademicPlanCell.h
//  RanepaApp
//
//  Created by Росляков Виктор on 09.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AcademicPlanCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *hoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *gradeLabel;

@end
