//
//  PostChangeViewController.m
//  RanepaApp
//
//  Created by Росляков Виктор on 15.03.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "PostChangeViewController.h"
#import "Profile.h"
#import "PostChangeFetch.h"

@interface PostChangeViewController ()

@property (strong, nonatomic) NSArray *pickerData;

@end

@implementation PostChangeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.commentTextView.layer.borderWidth = 0.5f;
    self.commentTextView.layer.cornerRadius =3.0f;
    self.commentTextView.layer.borderColor = [[UIColor grayColor] CGColor];
    self.pickerData = @[@"Добавление", @"Изменение", @"Удаление"];
    self.typePickerView;
}
- (IBAction)tapOnFinishButton:(id)sender {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString* discription = self.commentTextView.text;
    NSString* title =self.titleTextField.text;
    NSString* uid_obuch = [userDefaults objectForKey:@"UID_Obuch"];
    NSInteger number = [self.typePickerView selectedRowInComponent:0];
    NSString *numberString = [NSString stringWithFormat: @"%ld", (long)number];
    if(title && title.length > 0 && discription && discription.length > 0){
        NSDictionary *arrayLogin = [NSDictionary dictionaryWithObjectsAndKeys:
                                    //@" ",@"Ar_Files",
                                    discription,@"Discription",
                                    title,@"Field",
                                    uid_obuch,@"UID_Obuch",
                                    @"0",@"UID_Status",
                                    numberString,@"UID_Type",
                                    nil];
        NSError * error = nil;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:arrayLogin options:NSJSONReadingMutableContainers error:&error];
        NSString* newStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        PostChangeFetch *post = [[PostChangeFetch alloc]initWithDelegate:self];
        [post fetchData:newStr:arrayLogin];
    }
    else{
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Заполните все поля"
                                                                       message:@" "
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.pickerData.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return _pickerData[row];
}
- (void)textViewDidChange:(UITextView *)textView
{
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
}
- (void)didCompleteFetchPostWithError{
    
}
- (void)didCompletedFetchPost{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Заявка подана"
                                                                   message:@" "
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}
@end
