//
//  HistoryTableViewCell.h
//  RanepaApp
//
//  Created by Росляков Виктор on 06.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewCellProtocol.h"

@interface HistoryTableViewCell : UITableViewCell <TableViewCellProtocol>

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *resultLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;

@end
