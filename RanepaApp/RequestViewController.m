//
//  RequestViewController.m
//  RanepaApp
//
//  Created by Росляков Виктор on 17.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "RequestViewController.h"
#import "PracticeTableViewCell.h"
#import "Request.h"
#import "HelpFetch.h"
#import "FaqFetch.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface RequestViewController()

@property(nonatomic,strong)NSArray* requestArray;

@end

@implementation RequestViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    HelpFetch *helpFetch = [[HelpFetch alloc]initWithDelegate:self];
    [helpFetch getHelpData];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
    
}
-(CGFloat)tableView:(UITableView*)tableView
heightForHeaderInSection:(NSInteger)section {
//    if (section == 0) {
//        return 6.0;
//    }
    
    return 1.0;
}

//- (CGFloat)tableView:(UITableView*)tableView
//heightForFooterInSection:(NSInteger)section {
//    return 5.0;
//}
//
//- (UIView*)tableView:(UITableView*)tableView
//viewForHeaderInSection:(NSInteger)section {
//    return [[UIView alloc] initWithFrame:CGRectZero];
//}
//
//- (UIView*)tableView:(UITableView*)tableView
//viewForFooterInSection:(NSInteger)section {
//    return [[UIView alloc] initWithFrame:CGRectZero];
//}
- (void)didCompleteWithError:(NSError*)error{
//    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Ошибка"
//                                                                   message:@"Проблемы при загрузке данных."
//                                                            preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
//                                                          handler:^(UIAlertAction * action) {}];
//    
//    [alert addAction:defaultAction];
//    [self presentViewController:alert animated:YES completion:nil];
    int yPosition = 10;
    UILabel *label = [[UILabel alloc] initWithFrame: CGRectMake(0, yPosition, self.view.frame.size.width, 0)];
    [label setText: @"Отстутствуют данные о частых вопросах"];
    [label setBackgroundColor: [UIColor clearColor]];
    [label setNumberOfLines: 0];
    [label sizeToFit];
    [label setCenter: CGPointMake(self.view.center.x, self.view.center.y)];
    [[self view] addSubview:label];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PracticeTableViewCell* cell =[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    Request *request = self.requestArray[indexPath.section];
    cell.positionLabel.text = request.Cat;
    cell.companyLabel.text = request.Create;
    cell.dateLabel.text = request.Status;
    if ([request.Status isEqualToString: @"Выполнена"]) {
        cell.dateLabel.textColor = UIColorFromRGB(0x659969);
    }
    if ([request.Status isEqualToString:@"Отклонена"]) {
        cell.dateLabel.textColor = UIColorFromRGB(0x6D1416);
    }
    
    return cell;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.requestArray count];
}
- (void)didCompletedHelpFetch:(id)requestArray{
    self.requestArray = requestArray;
    [self.requestTableView reloadData];
    
}

@end
