//
//  AchievementsViewController.m
//  RanepaApp
//
//  Created by Росляков Виктор on 16.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "AchievementsViewController.h"
#import "SWRevealViewController.h"
#import "AchievementsCell.h"
#import "AchievementsFetch.h"
#import "Achievements.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface AchievementsViewController()

@property (nonatomic, strong)NSMutableArray *achievementsDictionary;

@end


@implementation AchievementsViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    self.achievementsTableView.rowHeight = UITableViewAutomaticDimension;
    self.achievementsTableView.estimatedRowHeight = 140;
    [_activityIndicator startAnimating];
    AchievementsFetch *af = [[AchievementsFetch alloc]initWithDelegate:self];
    [af getAchievementsData];
    UIFont *font = [UIFont fontWithName:@"helvetica-light" size:15.0];
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    UIColorFromRGB(0x6D1416),NSForegroundColorAttributeName,
                                    font,NSFontAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.title = @"Достижения";
    _achievementsTableView.rowHeight = UITableViewAutomaticDimension;
    _achievementsTableView.estimatedRowHeight = 60;
}

-(CGFloat)tableView:(UITableView*)tableView
heightForHeaderInSection:(NSInteger)section {
//    if (section == 0) {
//        return 6.0;
//    }
    
    return 1.0;
}

//- (CGFloat)tableView:(UITableView*)tableView
//heightForFooterInSection:(NSInteger)section {
//    return 5.0;
//}
//
//- (UIView*)tableView:(UITableView*)tableView
//viewForHeaderInSection:(NSInteger)section {
//    return [[UIView alloc] initWithFrame:CGRectZero];
//}
//
//- (UIView*)tableView:(UITableView*)tableView
//viewForFooterInSection:(NSInteger)section {
//    return [[UIView alloc] initWithFrame:CGRectZero];
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
    
}
- (void)didCompleteWithError:(NSError*)error{
//    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Ошибка"
//                                                                   message:@"Отстутствуют данные о достижениях."
//                                                            preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
//                                                          handler:^(UIAlertAction * action) {}];
//    
//    [alert addAction:defaultAction];
//    [_activityIndicator stopAnimating];
//
//    [self presentViewController:alert animated:YES completion:nil];
    
   
    int yPosition = 10;
    UILabel *label = [[UILabel alloc] initWithFrame: CGRectMake(0, yPosition, self.view.frame.size.width, 0)];
    [label setText: @"Отстутствуют данные о достижениях"];
    [label setBackgroundColor: [UIColor clearColor]];
    [label setNumberOfLines: 0];
    [label sizeToFit];
    [label setCenter: CGPointMake(self.view.center.x, self.view.center.y)];
    [[self view] addSubview:label];
    [_activityIndicator stopAnimating];
}
- (void)didCompletedAchievementsFetch:(id)dictionaryOfArray{
    
    NSMutableDictionary *tempDic = dictionaryOfArray;
    _achievementsDictionary = [[NSMutableArray alloc]init];
    NSArray* category = @[@"SAMOUPR",@"OBSHESTV",@"SPORT",@"TVORCH",@"STUDY",@"OTHER"];
    for(int i = 0; i<[category count];i++){
        if([tempDic objectForKey:category[i]]){
            [_achievementsDictionary addObjectsFromArray:[dictionaryOfArray objectForKey:category[i]]];
        }
    }
    [self.achievementsTableView reloadData];
    [_activityIndicator stopAnimating];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AchievementsCell* cell =[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.categoryLabel.text = ((Achievements*)[_achievementsDictionary objectAtIndex:indexPath.section]).Category;
    cell.descriptionLabel.text = ((Achievements*)[_achievementsDictionary objectAtIndex:indexPath.section]).Dost;
    cell.infoLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@",((Achievements*)[_achievementsDictionary objectAtIndex:indexPath.section]).Obj_Document.DataVidachi,
                                            ((Achievements*)[_achievementsDictionary objectAtIndex:indexPath.section]).Obj_Document.KemVydan,
                    ((Achievements*)[_achievementsDictionary objectAtIndex:indexPath.section]).Obj_Document.Seriya,
     ((Achievements*)[_achievementsDictionary objectAtIndex:indexPath.section]).Obj_Document.Nomer];
    //cell.infoLabel.text = @"25.06.2014 средней школой №423 Перовского района г. Москва В34 4398321";
    
    return cell;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.achievementsDictionary count];
}

@end
