//
//  AchievementsViewController.h
//  RanepaApp
//
//  Created by Росляков Виктор on 16.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "ViewController.h"

@interface AchievementsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UITableView *achievementsTableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
