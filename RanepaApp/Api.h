//
//  Api.h
//  RanepaApp
//
//  Created by Росляков Виктор on 12.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reg.h"
#import "ApiProtocol.h"

@interface Api : NSObject <NSURLSessionDataDelegate> 

-(void)registration:(NSString *)login pass:(NSString *)pass;

-(id)initWithDelegate:(id)delegate;

@property(nonatomic, weak)id <ApiProtocol> delegate;

@property (nonatomic, strong) Reg *reg;

@end
