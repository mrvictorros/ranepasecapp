//
//  ExamsViewController.m
//  RanepaApp
//
//  Created by Росляков Виктор on 03.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "ExamsViewController.h"
#import "RecordBookCell.h"
#import "RecordBook.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface ExamsViewController ()
@property NSArray* lessonNames ;
@property NSArray* teacherNames ;
@property NSArray* hours ;
@property NSArray* rating;
@end

@implementation ExamsViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    _lessonNames = [NSArray arrayWithObjects:@"Информатика и программирование", @"Геометрия и топология", nil];
    _teacherNames = [NSArray arrayWithObjects:@"Захаров А.Н.", @"Карулина Е.А.", nil];
    _hours = [NSArray arrayWithObjects:@"216 ч.", @"108 ч.", nil];
    _rating = [NSArray arrayWithObjects:@"Отлично", @"Хорошо", nil];
    _examsTableView.rowHeight = UITableViewAutomaticDimension;
    _examsTableView.estimatedRowHeight = 60;


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;

}
-(CGFloat)tableView:(UITableView*)tableView
        heightForHeaderInSection:(NSInteger)section {
//    if (section == 0) {
//        return 6.0;
//    }
    
    return 1.0;
}

//- (CGFloat)tableView:(UITableView*)tableView
//heightForFooterInSection:(NSInteger)section {
//    return 5.0;
//}

//- (UIView*)tableView:(UITableView*)tableView
//viewForHeaderInSection:(NSInteger)section {
//    return [[UIView alloc] initWithFrame:CGRectZero];
//}
//
//- (UIView*)tableView:(UITableView*)tableView
//viewForFooterInSection:(NSInteger)section {
//    return [[UIView alloc] initWithFrame:CGRectZero];
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RecordBookCell* cell =[tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.lessonNameLabel.text = ((RecordBook *)(_dataForTable[indexPath.section])).Discipline;
        cell.hoursLabel.text = [NSString stringWithFormat:@"%@ ч.",[((RecordBook *)(_dataForTable[indexPath.section])).Hours stringValue]];
        cell.ratingLabel.text = ((RecordBook *)(_dataForTable[indexPath.section])).Result;
    NSString *res = ((RecordBook *)(_dataForTable[indexPath.section])).Result;
    if (([res isEqualToString: @"Отлично"])||([res isEqualToString: @"Хорошо"])) {
        cell.ratingLabel.textColor = UIColorFromRGB(0x659969);
    }
    if ([res isEqualToString:@"Неудовлетворительно"]) {
        cell.ratingLabel.textColor = UIColorFromRGB(0x6D1416);
    }
        cell.teacherNameLabel.text = ((RecordBook *)(_dataForTable[indexPath.section])).Obj_Tutor.FIO;
    
    return cell;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.dataForTable count];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
