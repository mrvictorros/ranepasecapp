//
//  Obj_Tutor.h
//  RanepaApp
//
//  Created by Росляков Виктор on 17.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"

@interface Obj_Tutor : NSObject <EKMappingProtocol>

@property(nonatomic, copy)NSString* FIO; //строка ФИО преподавателя,
@property(nonatomic, copy)NSString* LoginAD;//строка Логин AD преподавателя,
@property(nonatomic, copy)NSString* UID_FIO;// строка UID преподавателя

+(EKObjectMapping *)objectMapping;

@end
