//
//  RecordBookViewController.m
//  RanepaApp
//
//  Created by Росляков Виктор on 03.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "RecordBookViewController.h"
#import "SWRevealViewController.h"
#import <PFNavigationDropdownMenu/PFNavigationDropdownMenu.h>
#import "RecordBookFetch.h"
#import "ExamsViewController.h"
#import "CreditsViewController.h"
#import "OtherViewController.h"
#import "AllDesciplinesViewController.h"
#import "RecordBook.h"
#import "Periodss.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface RecordBookViewController ()

@property (nonatomic, strong) NSArray* recordBookArray;

@property (nonatomic, strong) NSArray* periodsArray;

@property (nonatomic, strong) PFNavigationDropdownMenu *menuView;

@property NSUInteger semIndex;

@property (nonatomic, strong) AllDesciplinesViewController * allVC;

@end

@implementation RecordBookViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    SWRevealViewController *revealViewController = self.revealViewController;
    RecordBookFetch * recordBookFetch = [[RecordBookFetch alloc]initWithDelegate:self];
    [recordBookFetch getBookData];
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
   // NSArray *items = @[@"1 Семестр", @"2 Семестр", @"3 Семестр", @"4 Семестр", @"5 Семестр", @"6 Семестр", @"7 Семестр", @"8 Семестр"];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    [UINavigationBar appearance].titleTextAttributes = @{NSForegroundColorAttributeName: UIColorFromRGB(0x6D1416)};
    [UINavigationBar appearance].barStyle = UIBarStyleDefault;
    
    _allVC =  (AllDesciplinesViewController *)[((UINavigationController *)[[self viewControllers] objectAtIndex:0]) topViewController];
    [_allVC.activityIndicator startAnimating];
    
    }
- (void)didCompletedRecordBookFetch:(id)arrayOfPeriods:(id)arrayOfRecordBook{
    self.recordBookArray = arrayOfRecordBook;
    self.periodsArray = arrayOfPeriods;
    [_allVC.activityIndicator stopAnimating];
    [self setUpDropDown:[arrayOfPeriods count]];
    if (_semIndex == nil) {
        _semIndex = 0;
    }
    self.menuView.didSelectItemAtIndexHandler = ^(NSUInteger indexPath){
        NSLog(@"Did select item at index: %ld", indexPath);
        _semIndex = indexPath;
        [self setUpDataForTab];
    };
    [self setUpDataForTab];
}

- (void)didCompleteWithError:(NSError*)error{
//    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Ошибка"
//                                                                   message:@"Проблемы при загрузке данных."
//                                                            preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
//                                                          handler:^(UIAlertAction * action) {}];
//    
//    [alert addAction:defaultAction];
//    [self presentViewController:alert animated:YES completion:nil];
    int yPosition = 10;
    [_allVC.activityIndicator stopAnimating];
    UILabel *label = [[UILabel alloc] initWithFrame: CGRectMake(0, yPosition, self.view.frame.size.width, 0)];
    [label setText: @"Отстутствуют данные о зачетной книжке"];
    [label setBackgroundColor: [UIColor clearColor]];
    [label setNumberOfLines: 0];
    [label sizeToFit];
    [label setCenter: CGPointMake(self.view.center.x, self.view.center.y)];
    [[self view] addSubview:label];
}

-(void)setUpDropDown:(NSInteger)val{
    NSMutableArray *items =[NSMutableArray new];
    
    for(NSInteger i=0;i<val;i++){
        [items addObject:[NSString stringWithFormat:@"%@ %@",((Periodss*)[_periodsArray objectAtIndex:i]).s,((Periodss*)[_periodsArray objectAtIndex:i]).y]];
        
    }

    _menuView = [[PFNavigationDropdownMenu alloc] initWithFrame:CGRectMake(0, 0, 300, 44)
                                                                                   title:items.firstObject
                                                                                   items:items
                                                                           containerView:self.view];
    
    _menuView.arrowImage = [UIImage imageNamed:@"arrow_down-icon-black"];
    _menuView.cellTextLabelFont =[UIFont fontWithName:@"helvetica-light" size:15.0];
    _menuView.cellTextLabelColor = UIColorFromRGB(0x6D1416);
    self.navigationItem.titleView = _menuView;

}
-(void)setUpDataForTab{
    ExamsViewController *examsVC =  (ExamsViewController *)[((UINavigationController *)[[self viewControllers] objectAtIndex:1]) topViewController];
    CreditsViewController *creditsVC =  (CreditsViewController *)[((UINavigationController *)[[self viewControllers] objectAtIndex:2]) topViewController];
    OtherViewController * otherVC =  (OtherViewController *)[((UINavigationController *)[[self viewControllers] objectAtIndex:3]) topViewController];
    AllDesciplinesViewController * allVC =  (AllDesciplinesViewController *)[((UINavigationController *)[[self viewControllers] objectAtIndex:0]) topViewController];
    NSString *sem = nil;
    if([self.periodsArray count]>0){
         sem = ((Periodss*)[_periodsArray objectAtIndex:_semIndex]).s;
    }
    NSMutableArray *arrayForExams = [NSMutableArray new];
    NSMutableArray *arrayForCredits = [NSMutableArray new];
    NSMutableArray *arrayForOther = [NSMutableArray new];
    NSMutableArray *arrayForAll = [NSMutableArray new];

    
    
    for (NSInteger i = 0; i<[self.recordBookArray count]; i++) {
        if(([((RecordBook *)(self.recordBookArray [i])).Type isEqualToString: @"Экзамен"])&&([((RecordBook *)self.recordBookArray[i]).Semestr isEqualToString: sem])){
            [arrayForExams addObject:self.recordBookArray [i]];
            [arrayForAll addObject:self.recordBookArray [i]];
            //[examsVC.examsTableView reloadData];
        }else if (([((RecordBook *)(self.recordBookArray[i])).Type isEqualToString: @"Зачет"])&&([((RecordBook *)self.recordBookArray[i]).Semestr isEqualToString: sem])){
            [arrayForCredits addObject:self.recordBookArray[i]];
            [arrayForAll addObject:self.recordBookArray [i]];
        }else if (([((RecordBook *)(self.recordBookArray[i])).Type isEqualToString: @"Зачет с оценкой"])&&([((RecordBook *)self.recordBookArray[i]).Semestr isEqualToString: sem])){
            [arrayForCredits addObject:self.recordBookArray[i]];
            [arrayForAll addObject:self.recordBookArray [i]];
        }
        else if ([((RecordBook *)self.recordBookArray[i]).Semestr isEqualToString: sem]){
            [arrayForOther addObject:self.recordBookArray[i]];
            [arrayForAll addObject:self.recordBookArray [i]];
        }
    }
    examsVC.dataForTable = arrayForExams;
    [examsVC.examsTableView reloadData];
    creditsVC.dataForTable = arrayForCredits;
    [creditsVC.creditsTableView reloadData];
    otherVC.dataForTable  = arrayForOther;
    [otherVC.otherTableview reloadData];
    allVC.dataForTable = arrayForAll;
    [allVC.allDesciplinesTableView reloadData];
    
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
