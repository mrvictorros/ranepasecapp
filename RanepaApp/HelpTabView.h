//
//  HelpTabView.h
//  RanepaApp
//
//  Created by Росляков Виктор on 17.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpTabView : UITabBarController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@end
