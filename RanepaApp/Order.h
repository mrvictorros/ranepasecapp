//
//  Order.h
//  RanepaApp
//
//  Created by Росляков Виктор on 16.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"

@interface Order : NSObject

@property (strong,nonatomic) NSString* Date;
@property (strong,nonatomic) NSString* Description;
@property (strong,nonatomic) NSString* Title;
@property (strong,nonatomic) NSString* UID_Obuch;
@property (strong,nonatomic) NSString* Type;
@property (strong,nonatomic) NSString* Subtype;

@property (strong,nonatomic) NSNumber* UID_Type;
@property (strong,nonatomic) NSNumber* UID_Subtype;

+(EKObjectMapping *)objectMapping;


@end
