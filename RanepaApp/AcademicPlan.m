//
//  AcademicPlan.m
//  RanepaApp
//
//  Created by Росляков Виктор on 22.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "AcademicPlan.h"

@implementation AcademicPlan
+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    id(^transformBlock1)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSNumber class]]) {
            return value;
        }
        return 0;
    };
    
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"UID_Control" toProperty:@"UID_Control" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Discipline" toProperty:@"UID_Discipline" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Draft" toProperty:@"UID_Draft" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_GodObuch" toProperty:@"UID_GodObuch" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Control" toProperty:@"Control" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"God" toProperty:@"God" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Semestr" toProperty:@"Semestr" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Discipline" toProperty:@"Discipline" withValueBlock:transformBlock];
        
        [mapping mapKeyPath:@"SelectGroup" toProperty:@"SelectGroup" withValueBlock:transformBlock1];
        [mapping mapKeyPath:@"Hours" toProperty:@"Hours" withValueBlock:transformBlock1];
        [mapping mapKeyPath:@"UID_Semestr" toProperty:@"UID_Semestr" withValueBlock:transformBlock1];
        
        [mapping mapPropertiesFromArray:@[
                                          @"CanSelect",
                                          @"Selectable",
                                          @"Selected"]];
        
    }];
}
@end
