//
//  NameCellObject.m
//  RanepaApp
//
//  Created by Росляков Виктор on 31.01.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "NameCellObject.h"
#import "NameTableViewCell.h"

@implementation NameCellObject

-(NSString *) typeOfCell{
    return @"NameCellObject";
}

+ (NSString*)identifierForReusableCell {
    return NSStringFromClass([NameTableViewCell class]);
}

@end
