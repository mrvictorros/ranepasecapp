//
//  Practice.h
//  RanepaApp
//
//  Created by Росляков Виктор on 10.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Obj_CompanyInfo.h"
#import "Obj_Tutor.h"
#import "Obj_Director.h"
#import "EasyMapping.h"


@interface Practice : NSObject <EKMappingProtocol>

@property(strong,nonatomic)NSString* positionPractice;
@property(strong,nonatomic)NSString* companyPractice;
@property(strong,nonatomic)NSString* datePractice;
@property(strong,nonatomic)NSString* placePractice;
@property(strong,nonatomic)NSString* dutiesPractice;
@property(strong,nonatomic)NSString* headCompanyPractice;
@property(strong,nonatomic)NSString* headRanepaPractice;
@property(strong,nonatomic)NSString* urlDocOne;
@property(strong,nonatomic)NSString* urlDocTwo;
@property(strong,nonatomic)NSString* urlDocThree;
@property(strong,nonatomic)NSString* Ar_demands;
@property(strong,nonatomic)NSString* Ar_files;
@property(strong,nonatomic)NSString* DateEnd;
@property(strong,nonatomic)NSString* nDateEnd;
@property(strong,nonatomic)NSString* DateStart;
@property(strong,nonatomic)NSString* nDateStart;
@property(strong,nonatomic)NSString* Department;
@property(strong,nonatomic)NSString* Feedback;
@property(strong,nonatomic)NSString* Goal;
@property(strong,nonatomic)NSString* StudentPost;
@property(strong,nonatomic)NSString* Tasks;
@property(strong,nonatomic)NSString* UID_Company;
@property(strong,nonatomic)NSString* UID_Draft;
@property(strong,nonatomic)NSString* UID_Obuch;
@property(strong,nonatomic)NSString* UID_Stage;
@property(strong,nonatomic)NSString* UID_Type;
@property(strong,nonatomic)NSString* Type;
@property(strong,nonatomic)NSString* Update;

@property(nonatomic, copy)NSNumber* step;

@property(nonatomic, strong)Obj_CompanyInfo* Obj_CompanyInfo;

@property(nonatomic, strong)Obj_Tutor *Obj_Curator;

@property(nonatomic, strong)Obj_Director *Obj_Responsable;

@property (nonatomic, readwrite) BOOL ReadOnly;

+(EKObjectMapping *)objectMapping;


@end
