//
//  SelectCellObject.h
//  RanepaApp
//
//  Created by Росляков Виктор on 31.01.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TableViewCellObjectProtocol.h"

@interface SelectCellObject : NSObject <TableViewCellObjectProtocol>

@property(nonatomic, strong)NSMutableArray *titleArray;
@property(nonatomic, strong)NSString *subTitleSelectEdit;
@property(nonatomic, assign)NSInteger selectedIndex;
@property(nonatomic,assign)BOOL isExpanded;

-(NSString *) typeOfCell;

+ (NSString*)identifierForReusableCell;

@end
