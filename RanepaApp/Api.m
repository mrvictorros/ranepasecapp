//
//  Api.m
//  RanepaApp
//
//  Created by Росляков Виктор on 12.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Api.h"
#import <UIKit/UIKit.h>
#import "SBJson/SBJson4.h"
#import "AESCrypt.h"
#import "ProfileHTTPCommunication.h"
#import <MagicalRecord/MagicalRecord.h>
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCrypto.h>
#import "NSString+AESCrypt.h"
#import <MagicalRecord/MagicalRecord.h>
#import "ApiProtocol.h"


@interface Api()

@property(nonatomic, copy) void(^successBlock)(NSData *);


@end


@implementation Api

-(id)initWithDelegate:(id)delegate{
    self = [super init];
    if(self){
        _delegate = delegate;
    }
    return self;
}

-(void)registration:(NSString *)login pass:(NSString *)pass{
    if (!self.reg) {
        self.reg = [Reg MR_createEntity];
        
    }
    self.reg.login = login;
    self.reg.password = pass;
    NSString *appCode = @"LK-iOS";
    NSString *simCode = @"16ctrhtnysq0rjl1";
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *did = [[UIDevice currentDevice] identifierForVendor].UUIDString;
    NSDictionary *arrayLogin = [NSDictionary dictionaryWithObjectsAndKeys:
                          login,@"login",
                          pass,@"password",
                          did,@"did",
                          nil];
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:arrayLogin options:NSJSONReadingMutableContainers error:&error];
    NSString* newStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSString *encryptedData = [newStr AES128EncryptWithKey:simCode];

    // Create NSData object
    
    // Get NSString from NSData object in Base64
    //NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    NSString *finalStr = [NSString stringWithFormat:@"%@=%@",encryptedData,appCode];
    
    
    
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    //request.HTTPMethod = @"POST";
//    
//    NSDictionary *dictionary = @{@"x": @"finalStr"};
//    NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary
//                                                   options:kNilOptions error:&error];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://lk-api.ranepa.ru/iosinit?x=%@",finalStr]];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                          delegate:self
                                                     delegateQueue:Nil];
    
    
    if (!error) {
        __weak typeof(self)weakSelf = self;
        NSURLSessionDataTask *downloadTask = [session dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        __strong typeof(self)strongSelf = weakSelf;
                                                        if (data==nil) {
                                                            NSError *error = [NSError errorWithDomain:@"ServerApiError"
                                                                                                 code:nil
                                                                                             userInfo:nil];
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                [strongSelf.delegate didCompleteLoginAutorisationWithError];
                                                            });
                                                            return;
                                                            
                                                        }
                                                                       NSArray *datat = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                        options:0
                                                                                                                          error:&error];
                                                                       NSLog(@"%@",datat);
                                                        if(datat[0][@"auth"] != nil){
                                                        NSNumber *ss = datat[0][@"auth"];
                                                        if(![ss boolValue]){
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                [strongSelf.delegate didCompleteLoginAutorisationWithError];
                                                            });
                                                            return;
                                                        }
                                                        }
                                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                                          [strongSelf.delegate didCompletedLoginAutorisation];
                                                                       });
                                                                       
                                                                   }];
        [downloadTask resume];
//        if(reg){
//            NSString *newAppCode = [NSString stringWithFormat:@"%@-%@",appCode,did];
//            NSString *md5 = [self md5:newAppCode];
//            NSString *newSimCode =[md5 substringFromIndex:[md5 length]-15];
//            
//        }
}
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
        if([challenge.protectionSpace.host isEqualToString:@"lk-api.ranepa.ru"]){
            NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
        }
    }
}

- (NSString *) md5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
}

//- (void)retrieveURL:(NSURL *)url successBlock:(void(^)(NSData *))successBlock
//{
//    // сохраняем данный successBlock для вызова позже
//    self.successBlock = successBlock;
//    
//    // создаем запрос, используя данный url
//    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
//    
//    // создаем сессию, используя дефолтную конфигурацию и устанавливая наш экземпляр класса как делегат
//    NSURLSessionConfiguration *conf =
//    [NSURLSessionConfiguration defaultSessionConfiguration];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:conf delegate:self delegateQueue:nil];
//    
//    // подготавливаем загрузку
//    NSURLSessionDownloadTask *task = [session downloadTaskWithRequest:request];
//    
//    // устанавливаем HTTP соединение
//    [task resume];
//}
//- (void) URLSession:(NSURLSession *)session
//       downloadTask:(NSURLSessionDownloadTask *)downloadTask
//didFinishDownloadingToURL:(NSURL *)location
//{
//    // получаем загруженные данные из локального хранилища
//    NSData *data = [NSData dataWithContentsOfURL:location];
//    
//    // гарантируем, что вызов successBlock происходит в главном потоке
//    dispatch_async(dispatch_get_main_queue(), ^{
//        // вызываем сохраненный ранее блок как колбэк
//        self.successBlock(data);
//    });
//}



@end
