//
//  ExamsViewController.h
//  RanepaApp
//
//  Created by Росляков Виктор on 03.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExamsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *examsTableView;

@property (nonatomic, strong) NSMutableArray *dataForTable;



@end
