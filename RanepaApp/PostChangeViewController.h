//
//  PostChangeViewController.h
//  RanepaApp
//
//  Created by Росляков Виктор on 15.03.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "ViewController.h"

@interface PostChangeViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>
@property (strong, nonatomic) IBOutlet UITextField *titleTextField;
@property (strong, nonatomic) IBOutlet UITextView *commentTextView;
@property (strong, nonatomic) IBOutlet UIPickerView *typePickerView;

@end
