//
//  LoginViewController.m
//  RanepaApp
//
//  Created by Росляков Виктор on 12.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "LoginViewController.h"
#import "Autorisation.h"
#import "Api.h"
#import "AppDelegate.h"
#import "ApiProtocol.h"
#import "ResetPassFetch.h"
#import "ResetPass.h"


@interface LoginViewController ()

@property(strong,nonatomic)NSString* login;
@property(strong,nonatomic)NSString* password;

@property(strong,nonatomic)Autorisation *autorisation;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    [_resetPassView setAlpha:0];

}
-(void)dismissKeyboard
{
    [_loginTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    [_loginXibTextField resignFirstResponder];
}

- (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha
{
    // Convert hex string to an integer
    unsigned int hexint = [self intFromHexString:hexStr];
    
    // Create color object, specifying alpha as well
    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexint & 0xFF))/255
                    alpha:alpha];
    
    return color;
}
- (unsigned int)intFromHexString:(NSString *)hexStr
{
    unsigned int hexInt = 0;
    
    // Create scanner
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    
    // Tell scanner to skip the # character
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    
    // Scan hex value
    [scanner scanHexInt:&hexInt];
    
    return hexInt;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)doneButtonAction:(id)sender {
    
    [self.activityIndicator startAnimating];
    _login  = [_loginTextField text];
    _password = [_passwordTextField text];

    Api* api = [[Api alloc]initWithDelegate:self];
    [api registration:_login pass:_password];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:self.login forKey:@"login"];
    [userDefaults synchronize];
    NSString *log = [userDefaults objectForKey:@"login"];
    NSLog(log);
}

- (IBAction)forgotPassAction:(id)sender {
    [_resetPassView setAlpha:1];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}

-(void)didCompletedAutorization {
    if([_autorisation.returnCode longLongValue] == 401){
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Ошибка авторизации"
                                                                   message:@"Попробуйте еще раз."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
        [self.activityIndicator stopAnimating];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];

    }
    if(_autorisation.publicKey == nil){
        NSLog(@"app not regestred");
    }
    else{
        NSLog(@"app regestred");
        AppDelegate *appDelegateTemp = [AppDelegate app];
        appDelegateTemp.window.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateInitialViewController];
        [self.activityIndicator stopAnimating];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setBool:TRUE forKey:@"authenticatedUser"];
        [userDefaults synchronize];
       
    }
    
}
- (void)didCompletedLoginAutorisation {
    _autorisation = [[Autorisation alloc]initWithDelegate:self];;
    [_autorisation autorisation];
}

- (void)didCompleteLoginAutorisationWithError{
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Ошибка авторизации"
                                                                       message:@"Попробуйте еще раз."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        [self.activityIndicator stopAnimating];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)doneXibButton:(id)sender {
    NSString* gettinLogin = _loginXibTextField.text;
    ResetPassFetch *resetPassFetch = [[ResetPassFetch alloc]initWithDelegate:self];
    [resetPassFetch doReset:gettinLogin];
}

- (IBAction)cancelXibButton:(id)sender {
    [_resetPassView setAlpha:0];
}
- (void)didCompletedResetPassFetch:(id)resetPass{
    [_resetPassView setAlpha:0];
    ResetPass* res =(ResetPass*)resetPass;
    if([[res Status] isEqualToString:@"Saved"]){
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Пароль выслан на e-mail"
                                                                       message:@" "
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Неверно введен логин"
                                                                       message:@" "
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)didCompleteWithError:(NSError*)error{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Ошибка восстановления пароля"
                                                                   message:@"Попробуйте еще раз."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}
@end
