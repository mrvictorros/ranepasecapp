//
//  Request.h
//  RanepaApp
//
//  Created by Росляков Виктор on 23.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"
#import "Obj_Moderator.h"


@interface Request : NSObject <EKMappingProtocol>

@property(nonatomic, copy)NSString* Comment;
@property(nonatomic, copy)NSString* Create;
@property(nonatomic, copy)NSString* Discription;
@property(nonatomic, copy)NSString* Field;
@property(nonatomic, copy)NSString* UID_Obuch;
@property(nonatomic, copy)NSString* UID_Type;
@property(nonatomic, copy)NSString* UID_Zayavka;
@property(nonatomic, copy)NSString* Cat;
@property(nonatomic, copy)NSString* Status;
@property(nonatomic, copy)NSString* Type;

@property (nonatomic, readwrite) BOOL Modified;

@property(nonatomic, copy)NSNumber* UID_Status;
@property(nonatomic, copy)NSNumber* UID_Cat;

@property (nonatomic, strong) Obj_Moderator *Obj_Moderator;

+(EKObjectMapping *)objectMapping;

@end
