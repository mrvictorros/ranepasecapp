//
//  OneTitleCellObject.m
//  RanepaApp
//
//  Created by Росляков Виктор on 06.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "OneTitleCellObject.h"

@implementation OneTitleCellObject

+ (NSString*)identifierForReusableCell {
    return NSStringFromClass([OneTitleTableViewCell class]);
}

@end
