//
//  PurchaseHistoryCellObjectsBuilder.m
//  RanepaApp
//
//  Created by Росляков Виктор on 07.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "PurchaseHistoryCellObjectsBuilder.h"
#import "OneTitleCellObject.h"
#import "DoubleViewCellObject.h"
#import "HistoryCellObject.h"
#import "Payments.h"

@implementation PurchaseHistoryCellObjectsBuilder

+ (NSArray *) buildCellObjectsWithPurchase:(Purchase *) purchase{
    NSMutableArray *tempCellObject = [NSMutableArray array];
    OneTitleCellObject *firstObject = [[OneTitleCellObject alloc]init];
    firstObject.titleName = @"Общие сведения";
    [tempCellObject addObject:firstObject];
    DoubleViewCellObject *secObject = [[DoubleViewCellObject alloc]init];
    secObject.titleLeft = purchase.CONTRACT;
    secObject.subTitleLeft = @"Номер договора";
    secObject.titleRight = [purchase.COST stringValue];
    secObject.subTitleRight = @"Общая стоимость";
    [tempCellObject addObject:secObject];
    DoubleViewCellObject *thirdObject = [[DoubleViewCellObject alloc]init];
    thirdObject.titleLeft = [purchase.PAYED stringValue];
    thirdObject.subTitleLeft = @"Оплачено";
    thirdObject.titleRight = [purchase.NEEDPAY stringValue];
    thirdObject.subTitleRight = @"Задолженность";
    [tempCellObject addObject:thirdObject];
    OneTitleCellObject *fourthObject = [[OneTitleCellObject alloc]init];
    fourthObject.titleName = @"История платежей";
    [tempCellObject addObject:fourthObject];
    for (int i=0; i<[purchase.PAYMENTS count]; i++) {
        NSString *objectName = [NSString stringWithFormat:@"%d%@",i,@"HistoryObject"];
        Payments *tempPayments = [purchase.PAYMENTS objectAtIndex:i];
        HistoryCellObject *temp = [[HistoryCellObject alloc]init];
        temp.titleName = [NSString stringWithFormat:@"%@ (%@)",[tempPayments.VALUE stringValue],tempPayments.METHOD];
        temp.dateText = tempPayments.DATE;
        temp.resultText = @" ";
        [tempCellObject addObject:temp];
    }
    NSArray *cellObjects  = [tempCellObject copy];
    return cellObjects;
}

@end
