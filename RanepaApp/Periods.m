//
//  Periods.m
//  RanepaApp
//
//  Created by Росляков Виктор on 02.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "Periods.h"

@implementation Periods

+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    id(^transformBlock1)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSNumber class]]) {
            return value;
        }
        return 0;
    };
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        
        [mapping mapKeyPath:@"DISCOUNT" toProperty:@"DISCOUNT" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"START" toProperty:@"START" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"END" toProperty:@"END" withValueBlock:transformBlock];
        
        [mapping mapKeyPath:@"COST" toProperty:@"COST" withValueBlock:transformBlock1];
        [mapping mapKeyPath:@"PENALTY" toProperty:@"PENALTY" withValueBlock:transformBlock1];
        [mapping mapKeyPath:@"PAYED" toProperty:@"PAYED" withValueBlock:transformBlock1];
        
        
    }];
}

@end
