//
//  DoubleViewTableViewCell.h
//  RanepaApp
//
//  Created by Росляков Виктор on 06.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewCellProtocol.h"

@interface DoubleViewTableViewCell : UITableViewCell <TableViewCellProtocol>

@property (strong, nonatomic) IBOutlet UILabel *titleLeftLabel;
@property (strong, nonatomic) IBOutlet UILabel *subTitleLeftLabel;
@property (strong, nonatomic) IBOutlet UILabel *titleRightLabel;
@property (strong, nonatomic) IBOutlet UILabel *subTitleRightLabel;

@end
