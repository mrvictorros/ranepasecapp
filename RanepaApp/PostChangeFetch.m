//
//  PostChangeFetch.m
//  RanepaApp
//
//  Created by Росляков Виктор on 16.03.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "PostChangeFetch.h"

@implementation PostChangeFetch

-(id)initWithDelegate:(id)delegate{
    self = [super init];
    if(self){
        _delegate = delegate;
    }
    return self;
}

-(void)fetchData:(NSString*)finalStr:(NSDictionary*)dic{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *stringUrl = [NSString stringWithFormat:@"https://lk-api.ranepa.ru/postChange?key=%@&data=%@",[userDefaults objectForKey:@"publicKey"],[self urlencode:finalStr]];
    NSError *error;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    NSDictionary *mapData = dic;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    [request setHTTPBody:postData];
    
    __weak typeof(self)weakSelf = self;
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        __strong typeof(self)strongSelf = weakSelf;
                                                        // десериализуем полученную информацию
                                                        NSArray *datat = [NSJSONSerialization JSONObjectWithData:data
                                                                                                         options:0
                                                                                                           error:&error];
                                                        NSLog(@"%@",datat);
        //                                                _returnCode= datat[0][@"original"][@"code"];
        //                                                NSString *status = datat[0][@"original"][@"status"];
                                                        long codeTest = 200;
        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            [strongSelf.delegate didCompletedFetchPost];
                                                        });
    }];
    
    [postDataTask resume];
//    NSURL *url = [NSURL URLWithString:stringUrl];
//    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration
//                                                      delegate:self
//                                                 delegateQueue:Nil];
//
//
//    __weak typeof(self)weakSelf = self;
//    NSURLSessionDataTask *downloadTask = [session dataTaskWithURL:url
//                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                __strong typeof(self)strongSelf = weakSelf;
//                                                // десериализуем полученную информацию
//                                                NSArray *datat = [NSJSONSerialization JSONObjectWithData:data
//                                                                                                 options:0
//                                                                                                   error:&error];
//                                                NSLog(@"%@",datat);
////                                                _returnCode= datat[0][@"original"][@"code"];
////                                                NSString *status = datat[0][@"original"][@"status"];
//                                                long codeTest = 200;
//                                                
//                                                dispatch_async(dispatch_get_main_queue(), ^{
//                                                    [strongSelf.delegate didCompletedFetchPost];
//                                                });
//                                            }];
//
//// 3
//[downloadTask resume];
//
};
- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
        if([challenge.protectionSpace.host isEqualToString:@"lk-api.ranepa.ru"]){
            NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
        }
    }
}
- (NSString *)urlencode:(NSString*)inpu {
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *)[inpu UTF8String];
    int sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

@end
