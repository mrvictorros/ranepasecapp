//
//  FaqTableViewCell.h
//  RanepaApp
//
//  Created by Росляков Виктор on 30.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FaqTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel * question;

@end
