//
//  Order.m
//  RanepaApp
//
//  Created by Росляков Виктор on 16.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "Order.h"

@implementation Order

+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    id(^transformBlock1)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSNumber class]]) {
            return value;
        }
        return 0;
    };
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        
        [mapping mapKeyPath:@"Date" toProperty:@"Date" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Description" toProperty:@"Description" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Title" toProperty:@"Title" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Obuch" toProperty:@"UID_Obuch" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Type" toProperty:@"Type" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Subtype" toProperty:@"Subtype" withValueBlock:transformBlock];
        
        [mapping mapKeyPath:@"UID_Type" toProperty:@"UID_Type" withValueBlock:transformBlock1];
        [mapping mapKeyPath:@"UID_Subtype" toProperty:@"UID_Subtype" withValueBlock:transformBlock1];
        

        
        
    }];
}

@end
