//
//  TableViewCellProtocol.h
//  RanepaApp
//
//  Created by Росляков Виктор on 31.01.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

@protocol TableViewCellProtocol <NSObject>

- (void)updateCellWithObject:(id)object;

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;

@end
