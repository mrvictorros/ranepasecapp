//
//  Discount.h
//  RanepaApp
//
//  Created by Росляков Виктор on 02.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"


@interface Discount : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *TOTAL;
@property (nonatomic, copy) NSString *ALL;

+(EKObjectMapping *)objectMapping;

@end
