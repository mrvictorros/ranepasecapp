//
//  PracticeTableViewCell.h
//  RanepaApp
//
//  Created by Росляков Виктор on 07.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PracticeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
