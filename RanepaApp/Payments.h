//
//  Payments.h
//  RanepaApp
//
//  Created by Росляков Виктор on 02.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"


@interface Payments : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *DATE;
@property (nonatomic, copy) NSString *METHOD;
@property (nonatomic, copy) NSString *PAYER;

@property (nonatomic, copy) NSNumber *PENYA;
@property (nonatomic, copy) NSNumber *VALUE;

+(EKObjectMapping *)objectMapping;

@end
