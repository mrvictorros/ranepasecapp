//
//  ScheduleTableViewCell.h
//  RanepaApp
//
//  Created by Росляков Виктор on 07.08.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lessonNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *hoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *roomLabel;
@property (weak, nonatomic) IBOutlet UILabel *teacherLaber;
@property (weak, nonatomic) IBOutlet UILabel *typeOfLessonLabel;
@property (strong, nonatomic) IBOutlet UILabel *statusOfLessonLabel;


@end
