//
//  PracticeDataService.h
//  RanepaApp
//
//  Created by Росляков Виктор on 10.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Practice.h"

@interface PracticeDataService : NSObject

@property(nonatomic,readonly) NSMutableArray* practiceArray;

-(NSUInteger)practiceCount;

-(Practice *)practiceAtIndex: (NSUInteger)index;

@end
