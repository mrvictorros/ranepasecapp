//
//  TableViewCellObjectProtocol.h
//  RanepaApp
//
//  Created by Росляков Виктор on 31.01.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

@protocol TableViewCellObjectProtocol <NSObject>

+(NSString*)identifierForReusableCell;

@end
