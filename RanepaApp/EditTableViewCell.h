//
//  EditTableViewCell.h
//  RanepaApp
//
//  Created by Росляков Виктор on 31.01.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditCellObject.h"
#import "TableViewCellProtocol.h"

@interface EditTableViewCell : UITableViewCell <TableViewCellProtocol, UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *textFieldEditCell;
@property (strong, nonatomic) IBOutlet UILabel *subTitleEditCell;

@end
