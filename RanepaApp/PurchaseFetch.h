//
//  PurchaseFetch.h
//  RanepaApp
//
//  Created by Росляков Виктор on 01.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PurchaseProtocol.h"

@interface PurchaseFetch : NSObject

- (void)getPurchaseData;

- (void)postPurchase:(NSString *)urlString;

-(id)initWithDelegate:(id)delegate;

@property(nonatomic, weak)id <PurchaseProtocol> delegate;

@end
