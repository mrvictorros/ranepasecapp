//
//  Purchase.h
//  RanepaApp
//
//  Created by Росляков Виктор on 02.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Discount.h"
#import "Payer.h"
#import "Periods.h"
#import "Payments.h"
#import "EasyMapping.h"

@interface Purchase : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSNumber *NEEDPAY;
@property (nonatomic, copy) NSNumber *PENYA;
@property (nonatomic, copy) NSNumber *PAYED;
@property (nonatomic, copy) NSNumber *COST;
@property (nonatomic, copy) NSNumber *DepCode;

@property (nonatomic, copy) NSString *CONTRACT;
@property (nonatomic, copy) NSString *FirstYear;
@property (nonatomic, copy) NSString *URL;

@property (nonatomic, strong) NSArray *PAYER;
@property (nonatomic, strong) NSArray *PERIODS;
@property (nonatomic, strong) NSArray *PAYMENTS;
@property (nonatomic, strong) NSArray *truePeriods;

@property (nonatomic, strong) Discount *DISCOUNT;

@property (nonatomic, assign) BOOL ENABLE;


+(EKObjectMapping *)objectMapping;


@end
