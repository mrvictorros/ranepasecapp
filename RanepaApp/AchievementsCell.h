//
//  AchievementsCell.h
//  RanepaApp
//
//  Created by Росляков Виктор on 17.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AchievementsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end
