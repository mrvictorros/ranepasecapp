//
//  Obj_Tutor.m
//  RanepaApp
//
//  Created by Росляков Виктор on 17.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "Obj_Tutor.h"

@implementation Obj_Tutor

+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"FIO" toProperty:@"FIO" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"LoginAD" toProperty:@"LoginAD" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_FIO" toProperty:@"UID_FIO" withValueBlock:transformBlock];
        
        
        
    }];
}

@end
