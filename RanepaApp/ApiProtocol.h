//
//  ApiProtocol.h
//  RanepaApp
//
//  Created by Росляков Виктор on 27.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

@protocol ApiProtocol

- (void)didCompletedLoginAutorisation;

- (void)didCompleteLoginAutorisationWithError;

@end
