//
//  NameTableViewCell.m
//  RanepaApp
//
//  Created by Росляков Виктор on 31.01.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "NameTableViewCell.h"

static const CGFloat kCellHeight = 66.0f;

@implementation NameTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)updateCellWithObject:(NameCellObject *)object {
    
    _titleNameCell.text = object.titleName;
    _subTitleCell.text = object.subTitle;
    
}

+ (CGFloat)heightForObject:(id)object
               atIndexPath:(NSIndexPath *)indexPath
                 tableView:(UITableView *)tableView {
    return kCellHeight;
}

@end
