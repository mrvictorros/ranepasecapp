//
//  EditCellObject.h
//  RanepaApp
//
//  Created by Росляков Виктор on 31.01.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TableViewCellObjectProtocol.h"

@interface EditCellObject : NSObject <TableViewCellObjectProtocol>

@property(nonatomic, strong)NSString* textFieldEdit;
@property(nonatomic, strong)NSString* subTitleEdit;

-(NSString *) typeOfCell;

+ (NSString*)identifierForReusableCell;

@end
