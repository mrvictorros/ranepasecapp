//
//  AllDesciplinesViewController.h
//  RanepaApp
//
//  Created by Росляков Виктор on 15.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllDesciplinesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *allDesciplinesTableView;

@property (nonatomic, strong) NSMutableArray *dataForTable;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
