//
//  Faq.h
//  RanepaApp
//
//  Created by Росляков Виктор on 29.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"


@interface Faq : NSObject

@property(nonatomic, copy)NSNumber* ID;
@property(nonatomic, copy)NSNumber* IBLOCK_ID;
@property(nonatomic, copy)NSNumber* IBLOCK_SECTION_ID;

@property(nonatomic, copy)NSString* NAME;
@property(nonatomic, copy)NSString* PREVIEW_TEXT;
@property(nonatomic, copy)NSString* DETAIL_TEXT;

+(EKObjectMapping *)objectMapping;

@end
