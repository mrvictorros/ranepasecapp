//
//  Obj_Director.m
//  RanepaApp
//
//  Created by Росляков Виктор on 22.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "Obj_Director.h"

@implementation Obj_Director

+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"FIO" toProperty:@"FIO" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Email" toProperty:@"Email" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Phone" toProperty:@"Phone" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Post" toProperty:@"Post" withValueBlock:transformBlock];
        
    }];
}

@end
