//
//  OrderFetch.h
//  RanepaApp
//
//  Created by Росляков Виктор on 16.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderProtocol.h"

@interface OrderFetch : NSObject

- (void)getOrderData;

-(id)initWithDelegate:(id)delegate;

@property(nonatomic, weak)id <OrderProtocol> delegate;

@end
