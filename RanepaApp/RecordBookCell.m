//
//  RecordBookCell.m
//  RanepaApp
//
//  Created by Росляков Виктор on 04.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "RecordBookCell.h"

@implementation RecordBookCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
