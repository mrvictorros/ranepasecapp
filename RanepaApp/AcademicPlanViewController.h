//
//  AcademicPlanViewController.h
//  RanepaApp
//
//  Created by Росляков Виктор on 09.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "ViewController.h"

@interface AcademicPlanViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UITableView *academicTableView;

@end
