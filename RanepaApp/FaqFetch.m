//
//  FaqFetch.m
//  RanepaApp
//
//  Created by Росляков Виктор on 29.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "FaqFetch.h"
#import "EasyMapping.h"
#import "Faq.h"


@implementation FaqFetch

-(id)initWithDelegate:(id)delegate{
    self = [super init];
    if(self){
        _delegate = delegate;
    }
    return self;
}

- (void)getFaqData{
    // 1
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults objectForKey:@"url"];
    NSString *dataUrl = [NSString stringWithFormat:@"%@name=stud-faq&%@",[userDefaults objectForKey:@"url"],[userDefaults objectForKey:@"secondUrl"]];
    NSURL *url = [NSURL URLWithString:dataUrl];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                          delegate:self
                                                     delegateQueue:Nil];
    
    // 2
    __weak typeof(self)weakSelf = self;
    NSURLSessionDataTask *downloadTask = [session
                                          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              __strong typeof(self)strongSelf = weakSelf;
                                              
                                              NSArray *datat = [NSJSONSerialization JSONObjectWithData:data
                                                                                               options:0
                                                                                                 error:&error];
                                              NSDictionary *jsonError = datat[0][@"body"][@"ERROR"];
                                              if([jsonError count] ){
                                                  NSError *error = [NSError errorWithDomain:@"ServerApiError"
                                                                                       code:[jsonError objectForKey:@"CODE"]
                                                                                   userInfo:jsonError];
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [strongSelf.delegate didCompleteWithError:error];
                                                  });
                                                  return;
                                              }
                                              NSDictionary *json= datat[0][@"body"][@"ITEMS"];
                                              NSArray *requestArray = [EKMapper arrayOfObjectsFromExternalRepresentation:json
                                                                                                             withMapping:[Faq objectMapping]];
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [strongSelf.delegate didCompletedFaqFetch:requestArray];
                                                  
                                              });
                                              
                                          }];
    
    // 3
    [downloadTask resume];
    
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
        if([challenge.protectionSpace.host isEqualToString:@"lk-api.ranepa.ru"]){
            NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
        }
    }
}
@end
