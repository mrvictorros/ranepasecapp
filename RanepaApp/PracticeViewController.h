//
//  PracticeViewController.h
//  RanepaApp
//
//  Created by Росляков Виктор on 06.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "ViewController.h"

@interface PracticeViewController : ViewController <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UITableView *practiceTableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
