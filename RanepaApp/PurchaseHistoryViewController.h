//
//  PurchaseHistoryViewController.h
//  RanepaApp
//
//  Created by Росляков Виктор on 06.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentTableDelegate.h"

@interface PurchaseHistoryViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *payButton;

@property (strong, nonatomic) PaymentTableDelegate *tableDelegate;

@property (strong, nonatomic) IBOutlet UITableView *purchaseHistoryTableView;

@end
