//
//  OrderProtocol.h
//  RanepaApp
//
//  Created by Росляков Виктор on 16.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

@protocol OrderProtocol

- (void)didCompletedOrderFetch:(id)orderArray;

- (void)didCompleteWithError:(NSError*)error;

@end
