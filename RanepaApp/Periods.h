//
//  Periods.h
//  RanepaApp
//
//  Created by Росляков Виктор on 02.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"


@interface Periods : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *DISCOUNT;
@property (nonatomic, copy) NSString *START;
@property (nonatomic, copy) NSString *END;

@property (nonatomic, copy) NSNumber *COST;
@property (nonatomic, copy) NSNumber *PENALTY;
@property (nonatomic, copy) NSNumber *PAYED;

+(EKObjectMapping *)objectMapping;

@end
