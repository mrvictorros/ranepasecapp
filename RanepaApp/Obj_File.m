//
//  Obj_File.m
//  RanepaApp
//
//  Created by Росляков Виктор on 23.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "Obj_File.h"

@implementation Obj_File

+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"Link" toProperty:@"Link" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Type" toProperty:@"Type" withValueBlock:transformBlock];
        
        
        
    }];
}

@end
