//
//  PracticeDetailViewController.h
//  RanepaApp
//
//  Created by Росляков Виктор on 07.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "ViewController.h"
#import "Practice.h"

@interface PracticeDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) Practice *detailItem;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *placeLabel;
@property (weak, nonatomic) IBOutlet UILabel *headCompanyLabel;
@property (weak, nonatomic) IBOutlet UILabel *headRanepaLabel;
@property (weak, nonatomic) IBOutlet UILabel *dutiesLabel;
@property (weak, nonatomic) IBOutlet UILabel *docOneLabel;
@property (weak, nonatomic) IBOutlet UILabel *docTwoLabel;

- (IBAction)docOneButton:(UIButton *)sender;
- (IBAction)docTwoButton:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UITableViewCell *firstCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *secondCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *thirdCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *fourCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *fiveCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *sixCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *sevenCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *eightCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *nineCell;

@end
