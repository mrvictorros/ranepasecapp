//
//  HistoryCellObject.h
//  RanepaApp
//
//  Created by Росляков Виктор on 06.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HistoryTableViewCell.h"
#import "TableViewCellObjectProtocol.h"

@interface HistoryCellObject : NSObject <TableViewCellObjectProtocol>

@property(nonatomic, strong)NSString *titleName;
@property(nonatomic, strong)NSString *dateText;
@property(nonatomic, strong)NSString *resultText;

+ (NSString *)identifierForReusableCell;

@end
