//
//  QuestionsViewController.m
//  RanepaApp
//
//  Created by Росляков Виктор on 17.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "QuestionsViewController.h"
#import "FaqFetch.h"
#import "FaqTableViewCell.h"
#import "Faq.h"
#import "QuestionDetailViewController.h"

@implementation QuestionsViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    //FaqFetch *faqFetch = [[FaqFetch alloc]initWithDelegate:self];
    //[faqFetch getFaqData];
}

- (void)didCompletedFaqFetch:(id)arrayOfArray{
    
    self.faqArray = arrayOfArray;
    [self.questionsTableView reloadData];
    
}
- (void)didCompleteWithError:(NSError*)error{
//    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Ошибка"
//                                                                   message:@"Проблемы при загрузке данных."
//                                                            preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
//                                                          handler:^(UIAlertAction * action) {}];
//    
//    [alert addAction:defaultAction];
//    [self presentViewController:alert animated:YES completion:nil];
    int yPosition = 10;
    UILabel *label = [[UILabel alloc] initWithFrame: CGRectMake(0, yPosition, self.view.frame.size.width, 0)];
    [label setText: @"Отстутствуют данные о заявках"];
    [label setBackgroundColor: [UIColor clearColor]];
    [label setNumberOfLines: 0];
    [label sizeToFit];
    [label setCenter: CGPointMake(self.view.center.x, self.view.center.y)];
    [[self view] addSubview:label];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
    
}

-(CGFloat)tableView:(UITableView*)tableView
heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 6.0;
    }
    
    return 1.0;
}

- (CGFloat)tableView:(UITableView*)tableView
heightForFooterInSection:(NSInteger)section {
    return 5.0;
}

- (UIView*)tableView:(UITableView*)tableView
viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (UIView*)tableView:(UITableView*)tableView
viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FaqTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    Faq *faq = self.faqArray[indexPath.section];
    cell.question.text = faq.NAME;

    return cell;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.faqArray count];
}
#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"ShowQuestionsDetails"]) {
        NSIndexPath *indexPath = [self.questionsTableView indexPathForSelectedRow];
        Faq *faq = [self.faqArray objectAtIndex:indexPath.section];
        [[segue destinationViewController] setDetailItem:faq];
        //         DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        //         [controller setDetailItem:object];
        //         controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        //         controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}



@end
