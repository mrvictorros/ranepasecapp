//
//  RecordBookProtocol.h
//  RanepaApp
//
//  Created by Росляков Виктор on 21.11.16.
//  Copyright © 2017 RANEPA. All rights reserved.
//

@protocol ScheduleProtocol

- (void)didCompletedScheduleFetch:(id)arrayOfSchedule;

- (void)didCompleteWithError:(NSError*)error;


@end
