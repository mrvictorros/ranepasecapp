//
//  Request.m
//  RanepaApp
//
//  Created by Росляков Виктор on 23.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "Request.h"

@implementation Request

+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    id(^transformBlock1)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSNumber class]]) {
            return value;
        }
        return 0;
    };
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"Comment" toProperty:@"Comment" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Create" toProperty:@"Create" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Discription" toProperty:@"Discription" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Field" toProperty:@"Field" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Obuch" toProperty:@"UID_Obuch" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Type" toProperty:@"UID_Type" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Zayavka" toProperty:@"UID_Zayavka" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Cat" toProperty:@"Cat" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Status" toProperty:@"Status" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Type" toProperty:@"Type" withValueBlock:transformBlock];
        
        [mapping mapKeyPath:@"UID_Status" toProperty:@"UID_Status" withValueBlock:transformBlock1];
        [mapping mapKeyPath:@"UID_Cat" toProperty:@"UID_Cat" withValueBlock:transformBlock1];
        
        [mapping hasOne:[Obj_Moderator class] forKeyPath:@"Obj_Moderator"];
        
        
    }];
}

@end
