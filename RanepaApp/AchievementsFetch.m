//
//  AchievementsFetch.m
//  RanepaApp
//
//  Created by Росляков Виктор on 22.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "AchievementsFetch.h"
#import "EasyMapping.h"
#import "Achievements.h"


@implementation AchievementsFetch

-(id)initWithDelegate:(id)delegate{
    self = [super init];
    if(self){
        _delegate = delegate;
    }
    return self;
}

- (void)getAchievementsData{
    
    // 1
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults objectForKey:@"url"];
    NSString *dataUrl = [NSString stringWithFormat:@"%@name=stud-achiev&%@",[userDefaults objectForKey:@"url"],[userDefaults objectForKey:@"secondUrl"]];
    NSURL *url = [NSURL URLWithString:dataUrl];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                          delegate:self
                                                     delegateQueue:Nil];
    
    // 2
    __weak typeof(self)weakSelf = self;
    NSURLSessionDataTask *downloadTask = [session
                                          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              __strong typeof(self)strongSelf = weakSelf;
                                              if (data==nil) {
                                                  NSError *error = [NSError errorWithDomain:@"ServerApiError"
                                                                                       code:nil
                                                                                   userInfo:nil];
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [strongSelf.delegate didCompleteWithError:error];
                                                  });
                                                  return;
                                                  
                                              }
                                              NSArray *datat = [NSJSONSerialization JSONObjectWithData:data
                                                                                               options:0
                                                                                                 error:&error];
                                              NSMutableDictionary *finalArray = [NSMutableDictionary new];
                                              NSArray* category = @[@"SAMOUPR",@"OBSHESTV",@"SPORT",@"TVORCH",@"STUDY",@"OTHER"];
                                              NSDictionary *jsonError = nil;
                                              if([datat[0][@"body"][@"ERROR"] isKindOfClass:[NSDictionary class]]){
                                                  jsonError = datat[0][@"body"][@"ERROR"];
                                              }
                                              if([jsonError count] ){
                                                  NSError *error = [NSError errorWithDomain:@"ServerApiError"
                                                                                       code:[jsonError objectForKey:@"CODE"]
                                                                                   userInfo:jsonError];
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [strongSelf.delegate didCompleteWithError:error];
                                                  });
                                                  return;
                                              }
                                              NSDictionary *json= datat[0][@"body"][@"DATA"];
                                              for(int i = 0; i<[category count];i++){
                                              
                                              if([json objectForKey:category[i]]){
                                                  NSArray *tempArray =[EKMapper arrayOfObjectsFromExternalRepresentation:[json objectForKey:category[i]]
                                                                                                                    withMapping:[Achievements objectMapping]];
                                                  [finalArray setObject:tempArray forKey:category[i]];
                                                  
                                              }
                                                  
                                              }
                                              
                                       
                                                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                                                [strongSelf.delegate didCompletedAchievementsFetch:finalArray];
                                                                                            });
                                              
                                          }];
    
    // 3
    [downloadTask resume];
    
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
        if([challenge.protectionSpace.host isEqualToString:@"lk-api.ranepa.ru"]){
            NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
        }
    }
}

@end
