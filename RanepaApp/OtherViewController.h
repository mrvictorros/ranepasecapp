//
//  OtherViewController.h
//  RanepaApp
//
//  Created by Росляков Виктор on 01.12.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtherViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *otherTableview;

@property (nonatomic, strong) NSMutableArray *dataForTable;

@end
