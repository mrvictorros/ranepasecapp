//
//  Obj_CompanyInfo.m
//  RanepaApp
//
//  Created by Росляков Виктор on 22.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "Obj_CompanyInfo.h"

@implementation Obj_CompanyInfo

+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"Address" toProperty:@"Address" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"BankInfo" toProperty:@"BankInfo" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Name" toProperty:@"Name" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Type" toProperty:@"Type" withValueBlock:transformBlock];
        
        [mapping hasOne:[Obj_Director class] forKeyPath:@"Obj_Director"];
        [mapping hasOne:[Obj_Director class] forKeyPath:@"Obj_Responsable"];
    }];
}

@end
