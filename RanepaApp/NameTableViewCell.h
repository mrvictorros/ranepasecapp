//
//  NameTableViewCell.h
//  RanepaApp
//
//  Created by Росляков Виктор on 31.01.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NameCellObject.h"
#import "TableViewCellProtocol.h"

@interface NameTableViewCell : UITableViewCell <TableViewCellProtocol>

@property (strong, nonatomic) IBOutlet UILabel *titleNameCell;
@property (strong, nonatomic) IBOutlet UILabel *subTitleCell;

@end
