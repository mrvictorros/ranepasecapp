//
//  QuestionDetailViewController.m
//  RanepaApp
//
//  Created by Росляков Виктор on 30.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "QuestionDetailViewController.h"

@implementation QuestionDetailViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}
- (void)configureView {
    if (self.detailItem) {
        self.question.text = self.detailItem.NAME;
        [self.answerWebView loadHTMLString:self.detailItem.DETAIL_TEXT baseURL:nil];

        
    }
    
}
@end
