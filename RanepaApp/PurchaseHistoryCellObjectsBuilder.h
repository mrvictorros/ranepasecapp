//
//  PurchaseHistoryCellObjectsBuilder.h
//  RanepaApp
//
//  Created by Росляков Виктор on 07.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Purchase.h"

@interface PurchaseHistoryCellObjectsBuilder : NSObject

+ (NSArray *) buildCellObjectsWithPurchase:(Purchase *) purchase;

@end
