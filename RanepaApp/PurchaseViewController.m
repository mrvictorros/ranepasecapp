//
//  PurchaseViewController.m
//  RanepaApp
//
//  Created by Росляков Виктор on 31.01.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "PurchaseViewController.h"
#import "NameCellObject.h"
#import "SWRevealViewController.h"
#import "PurchaseFetch.h"
#import "PurchaseCellObjectsBuilder.h"
#import "NotificationConstants.h"
#import "SelectCellObject.h"
#import "EditCellObject.h"

@interface PurchaseViewController ()

@property(nonatomic,strong) NSArray *cellObjects;

@end

@implementation PurchaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    _cellObjects = [PurchaseCellObjectsBuilder buildCellObjectsWithPurchase:_purchase];
    self.tableDelegate = [[PaymentTableDelegate alloc] init];
    self.purchaseTableView.dataSource = self.tableDelegate;
    self.purchaseTableView.delegate = self.tableDelegate;
    self.tableDelegate.cellObjects = _cellObjects;
    self.purchaseTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.purchaseTableView reloadData];
    
}
- (IBAction)didPushFinalButton:(id)sender {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *FIO = [userDefaults objectForKey:@"profileFIO"];
    NSString *payer = [((SelectCellObject *)_cellObjects[1]).titleArray objectAtIndex:((SelectCellObject *)_cellObjects[1]).selectedIndex];
    NSString *period = [((SelectCellObject *)_cellObjects[6]).titleArray objectAtIndex:((SelectCellObject *)_cellObjects[6]).selectedIndex];
    NSString *institute = [userDefaults objectForKey:@"profileInstitute"];
    NSString *program = [userDefaults objectForKey:@"profileProgram"];
    NSString *year = [userDefaults objectForKey:@"profileYear"];
    NSString *price = ((EditCellObject *)_cellObjects[7]).textFieldEdit;
    NSString *loginAd = [userDefaults objectForKey:@"loginAD"];
    int priceint = [price intValue];
    if((priceint>0)&&(priceint<[_purchase.NEEDPAY intValue])){
    NSString *urlString = [NSString stringWithFormat:@"%@?Student=%@&Payer=%@&Institute=%@&Program=%@&Offert=%@&Year=%@&Period=%@&DepID=%@&Price=%@&Type=0&LoginAD=%@",_purchase.URL,FIO,payer,institute,program,_purchase.CONTRACT,year,period,[_purchase.DepCode stringValue],price,loginAd];
    NSString *escapedString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:escapedString];
    [[UIApplication sharedApplication] openURL:url];
    }
    else{
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Неверное значение суммы платежа"
                                                                       message:[NSString stringWithFormat:@"введите значение не превышающее %@",_purchase.NEEDPAY]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];

    }
    //Student (ФИО студента), Payer(выбранный плательщик), Institute(институт), Program(Образовательная программа), Offert (номер договора),  Year(год поступления), Period(выбранный период), DepID (идентификатор института). ты можешь в самом простом случае записать эти значения после адреса URL https://..../,,/?Student=A..&Payer=...
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateTableViewWithAnimation)
                                                 name:kSelectTableViewCellExpandNotification
                                               object:nil];

}

- (void)updateTableViewWithAnimation {
    [self.purchaseTableView beginUpdates];
    [self.purchaseTableView endUpdates];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kSelectTableViewCellExpandNotification
                                                  object:nil];
     [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
