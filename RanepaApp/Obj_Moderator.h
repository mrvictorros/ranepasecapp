//
//  Obj_Moderator.h
//  RanepaApp
//
//  Created by Росляков Виктор on 24.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"


@interface Obj_Moderator : NSObject <EKMappingProtocol>

@property(nonatomic, copy)NSString* UIDFIO;
@property(nonatomic, copy)NSString* FIO;
@property(nonatomic, copy)NSString* LAD;

@end
