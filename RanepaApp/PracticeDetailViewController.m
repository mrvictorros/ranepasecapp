//
//  PracticeDetailViewController.m
//  RanepaApp
//
//  Created by Росляков Виктор on 07.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "PracticeDetailViewController.h"

@interface PracticeDetailViewController ()

@end

@implementation PracticeDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configureView];
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowOffset = CGSizeMake(0.0, 1.0);
    shadow.shadowColor = [UIColor whiteColor];
    UIFont *font = [UIFont fontWithName:@"helvetica-light" size:15.0];
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil]
     setTitleTextAttributes:
     @{
       NSShadowAttributeName:shadow,
       NSFontAttributeName:font
       }
     forState:UIControlStateNormal];
    self.nameLabel.text = self.detailItem.StudentPost;
    self.companyLabel.text = self.detailItem.Obj_CompanyInfo.Name;
    self.dateLabel.text = [NSString stringWithFormat:@"%@-%@",self.detailItem.nDateStart,self.detailItem.nDateEnd];
    self.placeLabel.text = self.detailItem.Obj_CompanyInfo.Address;
    self.headCompanyLabel.text = self.detailItem.Obj_CompanyInfo.Obj_Director.FIO;
    self.headRanepaLabel.text = self.detailItem.Obj_Curator.FIO;
    self.dutiesLabel.text = self.detailItem.Tasks;
    self.docOneLabel.text = self.detailItem.urlDocOne;
    self.docTwoLabel.text = self.detailItem.urlDocTwo;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
    
}
- (void)configureView {
    if (self.detailItem) {
                self.nameLabel.text = self.detailItem.StudentPost;
                self.companyLabel.text = self.detailItem.Obj_CompanyInfo.Name;
                self.dateLabel.text = [NSString stringWithFormat:@"%@-%@",self.detailItem.nDateStart,self.detailItem.nDateEnd];
                self.placeLabel.text = self.detailItem.Obj_CompanyInfo.Address;
                self.headCompanyLabel.text = self.detailItem.Obj_CompanyInfo.Obj_Director.FIO;
                self.headRanepaLabel.text = self.detailItem.Obj_Curator.FIO;
                self.dutiesLabel.text = self.detailItem.Tasks;
                self.docOneLabel.text = self.detailItem.urlDocOne;
                self.docTwoLabel.text = self.detailItem.urlDocTwo;
//        self.nameLabel.text = self.detailItem.positionPractice;
//        self.companyLabel.text = self.detailItem.companyPractice;
//        self.dateLabel.text = self.detailItem.datePractice;
//        self.placeLabel.text = self.detailItem.placePractice;
//        self.headCompanyLabel.text = self.detailItem.headCompanyPractice;
//        self.headRanepaLabel.text = self.detailItem.headRanepaPractice;
//        self.dutiesLabel.text = self.detailItem.dutiesPractice;
//        self.docOneLabel.text = self.detailItem.urlDocOne;
//        self.docTwoLabel.text = self.detailItem.urlDocTwo;
        
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    int num = indexPath.row;
    UITableViewCell *cell;
    switch (num) {
        case 0:
            cell = self.firstCell;
            break;
        case 1:
            cell = self.secondCell;
            break;
        case 2:
            cell = self.thirdCell;
            break;
        case 3:
            cell = self.fourCell;
            break;
        case 4:
            cell = self.fiveCell;
            break;
        case 5:
            cell = self.sixCell;
            break;
        case 6:
            cell = self.sevenCell;
            break;
        case 7:
            cell = self.eightCell;
            break;
        case 8:
            cell = self.nineCell;
            break;
    }
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)docTwoButton:(UIButton *)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.yandex.ru/"]];
}


- (IBAction)docOneButton:(UIButton *)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.yandex.ru/"]];
}
@end
