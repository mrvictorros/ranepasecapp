//
//  DoubleViewCellObject.h
//  RanepaApp
//
//  Created by Росляков Виктор on 06.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DoubleViewTableViewCell.h"
#import "TableViewCellObjectProtocol.h"

@interface DoubleViewCellObject : NSObject <TableViewCellObjectProtocol>

@property(nonatomic, strong)NSString *titleLeft;
@property(nonatomic, strong)NSString *subTitleLeft;
@property(nonatomic, strong)NSString *titleRight;
@property(nonatomic, strong)NSString *subTitleRight;

+ (NSString *)identifierForReusableCell;

@end
