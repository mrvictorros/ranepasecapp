//
//  Obj_File.h
//  RanepaApp
//
//  Created by Росляков Виктор on 23.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"


@interface Obj_File : NSObject <EKMappingProtocol>

@property(strong,nonatomic)NSString* Link;
@property(strong,nonatomic)NSString* Type;

+(EKObjectMapping *)objectMapping;

@end
