//
//  PaymentTableDelegate.m
//  RanepaApp
//
//  Created by Росляков Виктор on 31.01.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "PaymentTableDelegate.h"
#import "TableViewCellProtocol.h"
#import "TableViewCellObjectProtocol.h"
#import "NameTableViewCell.h"
#import "SelectCellObject.h"
#import "EditCellObject.h"


@implementation PaymentTableDelegate 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_cellObjects count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    id<TableViewCellObjectProtocol> cellObject = self.cellObjects[indexPath.row];
    NSString *identifier = [[cellObject class] identifierForReusableCell];
    id<TableViewCellProtocol> cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    [cell updateCellWithObject:cellObject];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    id<TableViewCellObjectProtocol> cellObject = self.cellObjects[indexPath.row];
    NSString *nameClass = NSStringFromClass([cellObject class]);
    Class cellClass = NSClassFromString(nameClass);
    nameClass = [cellClass identifierForReusableCell];
    CGFloat height = [NSClassFromString(nameClass) heightForObject:cellObject
                                                       atIndexPath:indexPath
                                                         tableView:tableView];
    return height;
}

@end
