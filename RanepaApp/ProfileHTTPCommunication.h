//
//  ProfileHTTPCommunication.h
//  RanepaApp
//
//  Created by Росляков Виктор on 11.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProfileProtocol.h"

@interface ProfileHTTPCommunication : NSObject <NSURLSessionDataDelegate>

- (void)getProfileData;

-(id)initWithDelegate:(id)delegate;

@property(nonatomic, weak)id <ProfileProtocol> delegate;

@end
