//
//  RecordBookFetch.h
//  RanepaApp
//
//  Created by Росляков Виктор on 15.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RecordBookProtocol.h"

@interface RecordBookFetch : NSObject

- (void)getBookData;

-(id)initWithDelegate:(id)delegate;

@property(nonatomic, weak)id <RecordBookProtocol> delegate;

@end
