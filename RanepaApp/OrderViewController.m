//
//  OrderViewController.m
//  RanepaApp
//
//  Created by Росляков Виктор on 16.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "OrderViewController.h"
#import "SWRevealViewController.h"
#import "AchievementsCell.h"
#import "OrderFetch.h"
#import "Order.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface OrderViewController ()

@property (nonatomic, strong)NSArray *ordersArray;

@end

@implementation OrderViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    [_activityIndicator startAnimating];
    OrderFetch *of = [[OrderFetch alloc]initWithDelegate:self];
    [of getOrderData];
    UIFont *font = [UIFont fontWithName:@"helvetica-light" size:15.0];
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    UIColorFromRGB(0x6D1416),NSForegroundColorAttributeName,
                                    font,NSFontAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.title = @"Приказы";
    self.orderTableView.rowHeight = UITableViewAutomaticDimension;
    self.orderTableView.estimatedRowHeight = 60;
}
- (void)didCompleteWithError:(NSError*)error{
    int yPosition = 10;
    UILabel *label = [[UILabel alloc] initWithFrame: CGRectMake(0, yPosition, self.view.frame.size.width, 0)];
    [label setText: @"Отстутствуют данные о достижениях"];
    [label setBackgroundColor: [UIColor clearColor]];
    [label setNumberOfLines: 0];
    [label sizeToFit];
    [label setCenter: CGPointMake(self.view.center.x, self.view.center.y)];
    [[self view] addSubview:label];
    [_activityIndicator stopAnimating];
}

- (void)didCompletedOrderFetch:(id)dictionaryOfArray{
    self.ordersArray = dictionaryOfArray;
    [self.orderTableView reloadData];
    [_activityIndicator stopAnimating];
}

-(CGFloat)tableView:(UITableView*)tableView
heightForHeaderInSection:(NSInteger)section {
    return 1.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Order *order = [_ordersArray objectAtIndex:indexPath.section];
    AchievementsCell* cell =[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.categoryLabel.text = order.Title;
    cell.descriptionLabel.text = order.Description;
    cell.infoLabel.text = order.Date;
    //cell.infoLabel.text = @"25.06.2014 средней школой №423 Перовского района г. Москва В34 4398321";
    
    return cell;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [_ordersArray count];
}

@end
