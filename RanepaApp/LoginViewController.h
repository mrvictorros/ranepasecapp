//
//  LoginViewController.h
//  RanepaApp
//
//  Created by Росляков Виктор on 12.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AutorizationProtocol.h"
#import "ApiProtocol.h"

@interface LoginViewController : UIViewController <AutorizationProtocol>

@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (strong, nonatomic) IBOutlet UIButton *forgotPassButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UIView *resetPassView;


- (IBAction)doneButtonAction:(id)sender;
- (IBAction)forgotPassAction:(id)sender;

//ResetPasswordViewXib
@property (strong, nonatomic) IBOutlet UITextField *loginXibTextField;

- (IBAction)doneXibButton:(id)sender;
- (IBAction)cancelXibButton:(id)sender;


@end
