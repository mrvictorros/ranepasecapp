//
//  RecordBookFetch.m
//  RanepaApp
//
//  Created by Росляков Виктор on 15.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "RecordBookFetch.h"
#import "EasyMapping.h"
#import "RecordBook.h"
#import "Periodss.h"

@implementation RecordBookFetch

-(id)initWithDelegate:(id)delegate{
    self = [super init];
    if(self){
        _delegate = delegate;
    }
    return self;
}

- (void)getBookData{
    // 1
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults objectForKey:@"url"];
    NSString *dataUrl = [NSString stringWithFormat:@"%@name=stud-progress&%@",[userDefaults objectForKey:@"url"],[userDefaults objectForKey:@"secondUrl"]];
    NSURL *url = [NSURL URLWithString:dataUrl];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                          delegate:self
                                                     delegateQueue:Nil];
    
    // 2
    __weak typeof(self)weakSelf = self;
    NSURLSessionDataTask *downloadTask = [session
                                          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              __strong typeof(self)strongSelf = weakSelf;
                                              if (data==nil) {
                                                  NSError *error = [NSError errorWithDomain:@"ServerApiError"
                                                                                       code:nil
                                                                                   userInfo:nil];
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [strongSelf.delegate didCompleteWithError:error];
                                                  });
                                                  return;
                                                  
                                              }
                                              
                                              NSArray *datat = [NSJSONSerialization JSONObjectWithData:data
                                                                                               options:0
                                                                                                 error:&error];
                                              
                                              NSMutableArray *periodsArray = [NSMutableArray new] ;
                                              
                                              NSDictionary *jsonError = datat[0][@"body"][@"ERROR"];
                                              if([jsonError count] ){
                                                  NSError *error = [NSError errorWithDomain:@"ServerApiError"
                                                                                       code:[jsonError objectForKey:@"CODE"]
                                                                                   userInfo:jsonError];
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [strongSelf.delegate didCompleteWithError:error];
                                                  });
                                                  return;
                                              }
                                              
                                              NSDictionary *json= datat[0][@"body"][@"DATA"][@"Disc"];
                                              NSDictionary *json1= datat[0][@"body"][@"DATA"][@"Periods"];
                                              NSArray *carsArray = [EKMapper arrayOfObjectsFromExternalRepresentation:json
                                                                                                          withMapping:[RecordBook objectMapping]];
                                              NSArray *keysPeriods = [json1 allKeys];
                                              for (int i = [keysPeriods count]-1; i>=0; i--) {
                                                  [json1 objectForKey:keysPeriods[i]];
                                                  [periodsArray addObject:[EKMapper objectFromExternalRepresentation:[json1 objectForKey:keysPeriods[i]] withMapping:[Periodss objectMapping]]];
                                              }
                                              //for(NSInteger i=1;i<json.count+1;i++){
                                              //    NSString *string = [NSString stringWithFormat:@"%zd", i];
                                                  
                                                  
                                                  //[finalArray addObject:carsArray];
                                             // }
//                                              NSDictionary *jsonArDelo=datat[0][@"body"][@"DATA"][@"Ar_Delo"][0];
                                             
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [strongSelf.delegate didCompletedRecordBookFetch:periodsArray:carsArray];
                                                  
                                                                                                });
                                              
                                          }];
    
    // 3
    [downloadTask resume];
    
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
        if([challenge.protectionSpace.host isEqualToString:@"lk-api.ranepa.ru"]){
            NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
        }
    }
}

@end
