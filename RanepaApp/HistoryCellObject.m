//
//  HistoryCellObject.m
//  RanepaApp
//
//  Created by Росляков Виктор on 06.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "HistoryCellObject.h"

@implementation HistoryCellObject

+ (NSString*)identifierForReusableCell {
    return NSStringFromClass([HistoryTableViewCell class]);
}

@end
