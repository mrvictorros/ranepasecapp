//
//  AllDesciplinesViewController.m
//  RanepaApp
//
//  Created by Росляков Виктор on 15.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "AllDesciplinesViewController.h"
#import "RecordBookCell.h"
#import "RecordBook.h"

@interface AllDesciplinesViewController ()

@end

@implementation AllDesciplinesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    _allDesciplinesTableView.rowHeight = UITableViewAutomaticDimension;
    _allDesciplinesTableView.estimatedRowHeight = 60;
}
-(CGFloat)tableView:(UITableView*)tableView
heightForHeaderInSection:(NSInteger)section {
//    if (section == 0) {
//        return 6.0;
//    }
//    
    return 1.0;
}

//- (CGFloat)tableView:(UITableView*)tableView
//heightForFooterInSection:(NSInteger)section {
//    return 5.0;
//}
//
//- (UIView*)tableView:(UITableView*)tableView
//viewForHeaderInSection:(NSInteger)section {
//    return [[UIView alloc] initWithFrame:CGRectZero];
//}
//
//- (UIView*)tableView:(UITableView*)tableView
//viewForFooterInSection:(NSInteger)section {
//    return [[UIView alloc] initWithFrame:CGRectZero];
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RecordBookCell* cell =[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.lessonNameLabel.text = ((RecordBook *)(_dataForTable[indexPath.section])).Discipline;
    cell.teacherNameLabel.text = ((RecordBook *)(_dataForTable[indexPath.section])).Obj_Tutor.FIO;
    cell.hoursLabel.text = [NSString stringWithFormat:@"%@ ч.",[((RecordBook *)(_dataForTable[indexPath.section])).Hours stringValue]];
    cell.ratingLabel.text = ((RecordBook *)(_dataForTable[indexPath.section])).Result;
    
    return cell;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.dataForTable count];
}

@end
