//
//  Profile.h
//  RanepaApp
//
//  Created by Росляков Виктор on 01.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"

@interface Profile : NSObject <EKMappingProtocol>

@property(nonatomic, copy)NSString* DataRojdeniya;
@property(nonatomic, copy)NSString* Email;
@property(nonatomic, copy)NSString* Familiya;
@property(nonatomic, copy)NSString* Imya;
@property(nonatomic, copy)NSString* LoginAD;
@property(nonatomic, copy)NSString* Name;
@property(nonatomic, copy)NSString* Otchestvo;
@property(nonatomic, copy)NSString* Phone;
@property(nonatomic, copy)NSString* Photo;
@property(nonatomic, copy)NSString* Pol;
@property(nonatomic, copy)NSString* Surname;

@property(nonatomic, copy)NSArray* Ar_Delo;
@property(nonatomic, copy)NSArray* fcm_topics;

+(EKObjectMapping *)objectMapping;





@end
