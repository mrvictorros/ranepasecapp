//
//  Ar_Delo.h
//  RanepaApp
//
//  Created by Росляков Виктор on 02.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@interface Ar_Delo : NSObject <EKMappingProtocol>

@property(nonatomic, copy)NSString* Kurs;
@property(nonatomic, copy)NSNumber* Time;
@property(nonatomic, copy)NSString* UID_Fakultet;
@property(nonatomic, copy)NSString* UID_Filial;
@property(nonatomic, copy)NSString* UID_Forma;
@property(nonatomic, copy)NSString* UID_GodPostupleniya;
@property(nonatomic, copy)NSString* UID_Gruppa;
@property(nonatomic, copy)NSString* UID_Kafedra;
@property(nonatomic, copy)NSNumber* UID_Level;
@property(nonatomic, copy)NSString* UID_Napravlenie;
@property(nonatomic, copy)NSString* UID_Obuch;
@property(nonatomic, copy)NSString* UID_Osnova;
@property(nonatomic, copy)NSString* UID_Profil;
@property(nonatomic, copy)NSNumber* UID_Status;
@property(nonatomic, copy)NSString* Fakultet;
@property(nonatomic, copy)NSString* Filial;
@property(nonatomic, copy)NSString* Forma;
@property(nonatomic, copy)NSString* GodPostupleniya;
@property(nonatomic, copy)NSString* Gruppa;
@property(nonatomic, copy)NSString* Kafedra;
@property(nonatomic, copy)NSString* Level;
@property(nonatomic, copy)NSString* Napravlenie;
@property(nonatomic, copy)NSString* Osnova;
@property(nonatomic, copy)NSString* Profil;
@property(nonatomic, copy)NSString* Status;
@property(nonatomic, copy)NSString* Ar_UID_ScheduleGroups;

+(EKObjectMapping *)objectMapping;

@end
