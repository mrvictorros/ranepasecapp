//
//  Obj_Moderator.m
//  RanepaApp
//
//  Created by Росляков Виктор on 24.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "Obj_Moderator.h"

@implementation Obj_Moderator

+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"UIDFIO" toProperty:@"UIDFIO" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"FIO" toProperty:@"FIO" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"LAD" toProperty:@"LAD" withValueBlock:transformBlock];
    }];
}

@end
