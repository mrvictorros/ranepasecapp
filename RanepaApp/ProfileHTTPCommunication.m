//
//  ProfileHTTPCommunication.m
//  RanepaApp
//
//  Created by Росляков Виктор on 11.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "ProfileHTTPCommunication.h"
#import "Profile.h"
#import "EasyMapping.h"
#import "Ar_Delo.h"

@interface ProfileHTTPCommunication ()

@end

@implementation ProfileHTTPCommunication


-(id)initWithDelegate:(id)delegate{
    self = [super init];
    if(self){
        _delegate = delegate;
    }
    return self;
}

- (void)getProfileData{
    // 1
    
     NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults objectForKey:@"url"];
    NSString *dataUrl = [NSString stringWithFormat:@"%@name=stud-profile&Login_AD=%@&key=%@",[userDefaults objectForKey:@"url"],[userDefaults objectForKey:@"login"],[userDefaults objectForKey:@"publicKey"]];
    NSURL *url = [NSURL URLWithString:dataUrl];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                          delegate:self
                                                     delegateQueue:Nil];
    
    // 2
    __weak typeof(self)weakSelf = self;
    NSURLSessionDataTask *downloadTask = [session
                                          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              __strong typeof(self)strongSelf = weakSelf;
                                              if (data==nil) {
                                                  NSError *error = [NSError errorWithDomain:@"ServerApiError"
                                                                                       code:nil
                                                                                   userInfo:nil];
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [strongSelf.delegate didCompleteWithError:error];
                                                  });
                                                  return;
                                                  
                                              }
                                              NSArray *datat = [NSJSONSerialization JSONObjectWithData:data
                                                        options:0
                                                        error:&error];
                                              if (datat==nil) {
                                                  NSError *error = [NSError errorWithDomain:@"ServerApiError"
                                                                                       code:nil
                                                                                   userInfo:nil];
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [strongSelf.delegate didCompleteWithError:error];
                                                  });
                                                  return;

                                              }
                                              NSDictionary *jsonBody = datat[0][@"body"];
                                              if (![jsonBody isKindOfClass:[NSDictionary class]]) {
                                                  NSError *error = [NSError errorWithDomain:@"ServerApiError"
                                                                                       code:nil
                                                                                   userInfo:nil];
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [strongSelf.delegate didCompleteWithError:error];
                                                  });
                                                  return;
                                              }
                                              NSDictionary *jsonError = datat[0][@"body"][@"ERROR"];
                                              if([jsonError count] ){
                                                  NSError *error = [NSError errorWithDomain:@"ServerApiError"
                                                                                       code:[jsonError objectForKey:@"CODE"]
                                                                                   userInfo:jsonError];
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [strongSelf.delegate didCompleteWithError:error];
                                                  });
                                                  return;
                                              }
                                              
                                              
                                              NSDictionary *json= datat[0][@"body"][@"DATA"];
                                              NSDictionary *arDelo_json= datat[0][@"body"][@"DATA"][@"Ar_Delo"][0];
                                              Profile *profile = [EKMapper objectFromExternalRepresentation:json withMapping:[Profile objectMapping]];
                                              
                                              Ar_Delo *ar_delo = [EKMapper objectFromExternalRepresentation:arDelo_json withMapping:[Ar_Delo objectMapping]];
                                              
                                              if (ar_delo == nil){
                                                  NSError *error = [NSError errorWithDomain:@"ServerApiError"
                                                                                       code:[jsonError objectForKey:@"CODE"]
                                                                                   userInfo:jsonError];
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [strongSelf.delegate didCompleteWithError:error];
                                                  });
                                                  return;
                                              }
                                              NSLog(@"%@",profile.Email);
                                              NSLog(@"%@",ar_delo.Kurs);
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [strongSelf.delegate didCompletedProfileFetch:profile:ar_delo];
                                              });
                                              
                                          }];
    
    // 3
    [downloadTask resume];
    
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
        if([challenge.protectionSpace.host isEqualToString:@"lk-api.ranepa.ru"]){
            NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
        }
    }
}

@end
