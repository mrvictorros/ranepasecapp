//
//  Payments.m
//  RanepaApp
//
//  Created by Росляков Виктор on 02.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "Payments.h"

@implementation Payments

+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    id(^transformBlock1)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSNumber class]]) {
            return value;
        }
        return 0;
    };
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        
        [mapping mapKeyPath:@"DATE" toProperty:@"DATE" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"METHOD" toProperty:@"METHOD" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"PAYER" toProperty:@"PAYER" withValueBlock:transformBlock];
        
        [mapping mapKeyPath:@"PENYA" toProperty:@"PENYA" withValueBlock:transformBlock1];
        [mapping mapKeyPath:@"VALUE" toProperty:@"VALUE" withValueBlock:transformBlock1];
        
        
    }];
}

@end
