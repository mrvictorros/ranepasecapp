//
//  QuestionDetailViewController.h
//  RanepaApp
//
//  Created by Росляков Виктор on 30.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Faq.h"

@interface QuestionDetailViewController : UIViewController

@property (strong, nonatomic) Faq *detailItem;

@property (weak, nonatomic) IBOutlet UILabel *question;
@property (weak, nonatomic) IBOutlet UIWebView *answerWebView;

@end
