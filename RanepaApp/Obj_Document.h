//
//  Obj_Document.h
//  RanepaApp
//
//  Created by Росляков Виктор on 23.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"
#import "Obj_File.h"


@interface Obj_Document : NSObject <EKMappingProtocol>

@property(strong,nonatomic)NSString* DataVidachi;
@property(strong,nonatomic)NSString* KemVydan;

@property(nonatomic, copy)NSString* Nomer;
@property(nonatomic, copy)NSString* Seriya;

@property (nonatomic, strong) Obj_File *Obj_File;

+(EKObjectMapping *)objectMapping;

@end
