//
//  Payer.h
//  RanepaApp
//
//  Created by Росляков Виктор on 02.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"


@interface Payer : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *Name;
@property (nonatomic, readwrite) BOOL CanPay;

+(EKObjectMapping *)objectMapping;

@end
