//
//  AcademicPlanProtocol.h
//  RanepaApp
//
//  Created by Росляков Виктор on 22.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

@protocol AcademicPlanProtocol

- (void)didCompletedAcademicPlanFetch:(id)arrayOfPeriods:(id)arrayOfAcademicPlan;

- (void)didCompleteWithError:(NSError*)error;


@end