//
//  AcademicPlanViewController.m
//  RanepaApp
//
//  Created by Росляков Виктор on 09.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "AcademicPlanViewController.h"
#import "SWRevealViewController.h"
#import <PFNavigationDropdownMenu/PFNavigationDropdownMenu.h>
#import "AcademicPlanCell.h"
#import "AcademicPlanFetch.h"
#import "AcademicPlan.h"
#import "Periodss.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface AcademicPlanViewController()

@property (nonatomic, strong) NSArray* academicPlanArray;

@property (nonatomic, strong) NSMutableArray * arrayForTable;

@property (nonatomic, strong) NSArray* periodsArray;

@property (nonatomic, strong) PFNavigationDropdownMenu *menuView;

@property NSUInteger semIndex;

@end


@implementation AcademicPlanViewController


-(void)viewDidLoad{
    [super viewDidLoad];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    AcademicPlanFetch *academicFetch = [[AcademicPlanFetch alloc]initWithDelegate:self];
    [academicFetch getAcademicPlanData];
    self.academicTableView.rowHeight = UITableViewAutomaticDimension;
    self.academicTableView.estimatedRowHeight = 60;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    [UINavigationBar appearance].titleTextAttributes = @{NSForegroundColorAttributeName: UIColorFromRGB(0x6D1416)};
    [UINavigationBar appearance].barStyle = UIBarStyleDefault;
  
}
-(void)setUpDropDown:(NSInteger)val{
    NSMutableArray *items =[NSMutableArray new];
    for(NSInteger i=0;i<val;i++){
        [items addObject:[NSString stringWithFormat:@"%@ %@",((Periodss*)[_periodsArray objectAtIndex:i]).s,((Periodss*)[_periodsArray objectAtIndex:i]).y]];
        
    }
    _menuView = [[PFNavigationDropdownMenu alloc] initWithFrame:CGRectMake(0, 0, 300, 44)
                                                          title:items.firstObject
                                                          items:items
                                                  containerView:self.view];
    _menuView.arrowImage = [UIImage imageNamed:@"arrow_down-icon-black"];
    _menuView.cellTextLabelFont =[UIFont fontWithName:@"helvetica-light" size:15.0];
    _menuView.cellTextLabelColor = UIColorFromRGB(0x6D1416);
    self.navigationItem.titleView = _menuView;
    
    
}
- (void)didCompleteWithError:(NSError*)error{
//    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Ошибка"
//                                                                   message:@"Проблемы при загрузке данных."
//                                                            preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
//                                                          handler:^(UIAlertAction * action) {}];
//    
//    [alert addAction:defaultAction];
//    [self presentViewController:alert animated:YES completion:nil];
        int yPosition = 10;
        UILabel *label = [[UILabel alloc] initWithFrame: CGRectMake(0, yPosition, self.view.frame.size.width, 0)];
        [label setText: @"Отстутствуют данные об учебном плане"];
        [label setBackgroundColor: [UIColor clearColor]];
        [label setNumberOfLines: 0];
        [label sizeToFit];
        [label setCenter: CGPointMake(self.view.center.x, self.view.center.y-100)];
        [[self view] addSubview:label];
    
}

- (void)didCompletedAcademicPlanFetch:(id)arrayOfPeriods:(id)arrayOfAcademicPlan{
    self.academicPlanArray = arrayOfAcademicPlan;
    _arrayForTable = [NSMutableArray new];
    self.periodsArray = arrayOfPeriods;
    [self setUpDropDown:[_periodsArray count]];
    if (_semIndex == nil) {
        _semIndex = 0;
        if ([self.periodsArray count]>0) {
        NSString *sem = ((Periodss*)[_periodsArray objectAtIndex:0]).s;
        for (int i=0; i<[_academicPlanArray count]; i++) {
            if([((AcademicPlan *)([_academicPlanArray objectAtIndex:i])).Semestr isEqualToString:sem]){
                
                [_arrayForTable addObject:[_academicPlanArray objectAtIndex:i]];
            }
            
        }
        }

        [self.academicTableView reloadData];
    }
    self.menuView.didSelectItemAtIndexHandler = ^(NSUInteger indexPath){
        NSLog(@"Did select item at index: %ld", indexPath);
        _semIndex = indexPath;
        NSString *sem = ((Periodss*)[_periodsArray objectAtIndex:indexPath]).s;
        [_arrayForTable removeAllObjects];
        for (int i=0;i<[_academicPlanArray count]; i++) {
            if([((AcademicPlan *)([_academicPlanArray objectAtIndex:i])).Semestr isEqualToString:sem]){
                [_arrayForTable addObject:[_academicPlanArray objectAtIndex:i]];
            }

        }
        
        [self.academicTableView reloadData];
        
    };
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
    
}
-(CGFloat)tableView:(UITableView*)tableView
heightForHeaderInSection:(NSInteger)section {
//    if (section == 0) {
//        return 6.0;
//    }
    
    return 1.0;
}

//- (CGFloat)tableView:(UITableView*)tableView
//heightForFooterInSection:(NSInteger)section {
//    return 5.0;
//}
//
//- (UIView*)tableView:(UITableView*)tableView
//viewForHeaderInSection:(NSInteger)section {
//    return [[UIView alloc] initWithFrame:CGRectZero];
//}
//
//- (UIView*)tableView:(UITableView*)tableView
//viewForFooterInSection:(NSInteger)section {
//    return [[UIView alloc] initWithFrame:CGRectZero];
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
      AcademicPlanCell* cell =[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.nameLable.text = ((AcademicPlan *)([_arrayForTable objectAtIndex:indexPath.section])).Discipline;
    cell.gradeLabel.text = ((AcademicPlan *)([_arrayForTable objectAtIndex:indexPath.section])).Control;
    cell.hoursLabel.text = [NSString stringWithFormat:@"%@ ч.",[((AcademicPlan *)([_arrayForTable objectAtIndex:indexPath.section])).Hours stringValue]];
    
    return cell;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [_arrayForTable count];
}


@end
