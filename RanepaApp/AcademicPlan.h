//
//  AcademicPlan.h
//  RanepaApp
//
//  Created by Росляков Виктор on 22.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"

@interface AcademicPlan : NSObject <EKMappingProtocol>

@property(nonatomic, copy)NSString* UID_Control;
@property(nonatomic, copy)NSString* UID_Discipline;
@property(nonatomic, copy)NSString* UID_Draft;
@property(nonatomic, copy)NSString* UID_GodObuch;
@property(nonatomic, copy)NSString* Control;
@property(nonatomic, copy)NSString* God;
@property(nonatomic, copy)NSString* Semestr;
@property(nonatomic, copy)NSString* Discipline;

@property (nonatomic, readwrite) BOOL CanSelect;
@property (nonatomic, readwrite) BOOL Selectable;
@property (nonatomic, readwrite) BOOL Selected;

@property(nonatomic, copy)NSNumber* Hours;
@property(nonatomic, copy)NSNumber* SelectGroup;
@property(nonatomic, copy)NSNumber* UID_Semestr;

+(EKObjectMapping *)objectMapping;

@end
