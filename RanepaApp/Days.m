//
//  Days.m
//  RanepaApp
//
//  Created by Росляков Виктор on 07.08.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "Days.h"

@implementation Days

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[
                                          @"datetime"]];
        [mapping hasMany:[Schedule class] forKeyPath:@"discs"];
    }];
}

@end
