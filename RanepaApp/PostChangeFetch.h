//
//  PostChangeFetch.h
//  RanepaApp
//
//  Created by Росляков Виктор on 16.03.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiPostChange.h"

@interface PostChangeFetch : NSObject

-(id)initWithDelegate:(id)delegate;

@property(nonatomic, weak)id <ApiPostChange> delegate;

-(void)fetchData:(NSString*)finalStr:(NSDictionary*)dic;
@end
