//
//  ScheduleFetch.m
//  RanepaApp
//
//  Created by Росляков Виктор on 04.08.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "ScheduleFetch.h"
#import "EasyMapping.h"
#import "Schedule.h"
#import "Days.h"


@implementation ScheduleFetch

-(id)initWithDelegate:(id)delegate{
    self = [super init];
    if(self){
        _delegate = delegate;
    }
    return self;
}

- (void)getScheduleData:(int)offset:(NSString *)date{
    // 1
    NSString *dataUrl;
    NSString *offsetString = [NSString stringWithFormat:@"%d",offset];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    dataUrl = [NSString stringWithFormat:@"%@%@&key=%@&FILTERS=%@&STARTDATE=%@",@"https://lk-api.ranepa.ru/public/route?function=lk-bxcomponent&name=schedule&BY=group&OFFSET=",offsetString,[userDefaults objectForKey:@"publicKey"],[userDefaults objectForKey:@"Ar_UID_ScheduleGroups"],date];
    
    NSURL *url = [NSURL URLWithString:dataUrl];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                          delegate:self
                                                     delegateQueue:Nil];
    
    // 2
    __weak typeof(self)weakSelf = self;
    NSURLSessionDataTask *downloadTask = [session
                                          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              __strong typeof(self)strongSelf = weakSelf;
                                              if (data==nil) {
                                                  NSError *error = [NSError errorWithDomain:@"ServerApiError"
                                                                                       code:nil
                                                                                   userInfo:nil];
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [strongSelf.delegate didCompleteWithError:error];
                                                  });
                                                  return;
                                                  
                                              }
                                              NSDictionary *datat = [NSJSONSerialization JSONObjectWithData:data
                                                                                               options:0
                                                                                                 error:&error][0];
                                              NSDictionary *body = datat[@"body"];
                                              if (![body isKindOfClass:[NSDictionary class]]) {
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [strongSelf.delegate didCompleteWithError:[NSError new]];
                                                  });
                                                  return;
                                              }
                                              
                                              NSDictionary *json= body[@"Schedule"][@"days"];
                                              
                                              NSArray *daysArray = [EKMapper arrayOfObjectsFromExternalRepresentation:json
                                                                            withMapping:[Days objectMapping]];
                                              
                                              //for(NSInteger i=1;i<json.count+1;i++){
                                              //    NSString *string = [NSString stringWithFormat:@"%zd", i];
                                              
                                              
                                              //[finalArray addObject:carsArray];
                                              // }
                                              //                                              NSDictionary *jsonArDelo=datat[0][@"body"][@"DATA"][@"Ar_Delo"][0];
                                              
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [strongSelf.delegate didCompletedScheduleFetch:daysArray];
                                                  
                                              });
                                              
                                          }];
    
    // 3
    [downloadTask resume];
    
}
- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
        if([challenge.protectionSpace.host isEqualToString:@"lk-api.ranepa.ru"]){
            NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
        }
    }
}
@end
