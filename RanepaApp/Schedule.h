//
//  Schedule.h
//  RanepaApp
//
//  Created by Росляков Виктор on 04.08.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"

@interface Schedule : NSObject <EKMappingProtocol>

@property(nonatomic, strong)NSArray* groups;

@property(nonatomic, strong)NSString* UID_disc;
@property(nonatomic, strong)NSString* UID_type;
@property(nonatomic, strong)NSString* UID_lecturer;
@property(nonatomic, strong)NSString* UID_room;
@property(nonatomic, strong)NSString* comment;
@property(nonatomic, strong)NSString* startDate;
@property(nonatomic, strong)NSString* endDate;
@property(nonatomic, strong)NSString* status;

+(EKObjectMapping *)objectMapping;

@end
