//
//  EditCellObject.m
//  RanepaApp
//
//  Created by Росляков Виктор on 31.01.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "EditCellObject.h"
#import "EditTableViewCell.h"

@implementation EditCellObject

-(NSString *) typeOfCell{
    return @"EditCellObject";
}

+ (NSString*)identifierForReusableCell {
    return @"EditTableViewCell";
}

@end
