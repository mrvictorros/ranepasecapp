//
//  Autorisation.h
//  RanepaApp
//
//  Created by Росляков Виктор on 14.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AutorizationProtocol.h"

@class Reg;
@interface Autorisation : NSObject <NSURLSessionDataDelegate> 

-(void)autorisation;

-(id)initWithDelegate:(id)delegate;

@property(nonatomic, weak)id <AutorizationProtocol> delegate;

@property (nonatomic, strong) Reg *reg;

@property(strong,nonatomic)NSString* publicKey;
@property(strong,nonatomic)NSString* privateKey;
@property(strong,nonatomic)NSString *appCode;
@property(strong,nonatomic)NSString *simCode;
@property(strong,nonatomic)NSString *returnCode;


@end
