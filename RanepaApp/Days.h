//
//  Days.h
//  RanepaApp
//
//  Created by Росляков Виктор on 07.08.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"
#import "Schedule.h"

@interface Days : NSObject <EKMappingProtocol>

@property(nonatomic, strong)NSString* datetime;
@property(nonatomic, strong)NSArray* discs;


@end
