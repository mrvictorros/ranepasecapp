//
//  Achievements.m
//  RanepaApp
//
//  Created by Росляков Виктор on 23.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "Achievements.h"

@implementation Achievements

+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    id(^transformBlock1)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSNumber class]]) {
            return value;
        }
        return 0;
    };
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        
        [mapping mapKeyPath:@"UID_Draft" toProperty:@"UID_Draft" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Dost" toProperty:@"Dost" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Dost" toProperty:@"UID_Dost" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Category" toProperty:@"Category" withValueBlock:transformBlock];
        
        [mapping mapKeyPath:@"Ball" toProperty:@"Ball" withValueBlock:transformBlock1];
        
        [mapping hasOne:[Obj_Document class] forKeyPath:@"Obj_Document"];

        [mapping mapPropertiesFromArray:@[@"readonly"]];
        
        
    }];
}

@end
