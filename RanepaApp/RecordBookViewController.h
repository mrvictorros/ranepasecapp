//
//  RecordBookViewController.h
//  RanepaApp
//
//  Created by Росляков Виктор on 03.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecordBookProtocol.h"

@interface RecordBookViewController : UITabBarController <RecordBookProtocol>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@end
