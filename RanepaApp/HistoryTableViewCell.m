//
//  HistoryTableViewCell.m
//  RanepaApp
//
//  Created by Росляков Виктор on 06.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "HistoryTableViewCell.h"
#import "HistoryCellObject.h"

static const CGFloat kCellHeight = 66.0f;

@implementation HistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)updateCellWithObject:(HistoryCellObject *)object {
    
    _titleLabel.text = object.titleName;
    _dateLabel.text = object.dateText;
    _resultLabel.text = object.resultText;
    
}

+ (CGFloat)heightForObject:(id)object
               atIndexPath:(NSIndexPath *)indexPath
                 tableView:(UITableView *)tableView {
    return kCellHeight;
}

@end
