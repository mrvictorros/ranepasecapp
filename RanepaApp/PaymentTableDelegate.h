//
//  PaymentTableDelegate.h
//  RanepaApp
//
//  Created by Росляков Виктор on 31.01.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PaymentTableDelegate : NSObject <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, copy)NSArray *cellObjects;

@end
