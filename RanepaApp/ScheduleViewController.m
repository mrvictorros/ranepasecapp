 //
//  ScheduleViewController.m
//  RanepaApp
//
//  Created by Росляков Виктор on 26.07.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "ScheduleViewController.h"
#import "SWRevealViewController.h"
#import "ScheduleFetch.h"
#import "ScheduleTableViewCell.h"
#import "Days.h"
#import "Schedule.h"

@interface ScheduleViewController ()

@property NSDate* curDate;

@property NSString* curDateString;

@property NSArray *daysArray;
@property NSArray *scheduleArray;
@property NSMutableDictionary *arrayOfDates;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *calendarHeightConstraint;

@end

@implementation ScheduleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    _calendar.scope = FSCalendarScopeWeek;
    _calendar.locale = [NSLocale localeWithLocaleIdentifier:@"ru-RU"];
    self.scheduleTableView.rowHeight = UITableViewAutomaticDimension;
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:now];
    [components setHour:0];
    NSDate *today10am = [calendar dateFromComponents:components];
    _curDate = today10am;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *datet = [formatter stringFromDate:today10am];
    [_ActivityIndicator startAnimating];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    ScheduleFetch *scheduleFetch = [[ScheduleFetch alloc]initWithDelegate:self];
    [scheduleFetch getScheduleData:7:datet];
    _scheduleTableView.rowHeight = UITableViewAutomaticDimension;
    _scheduleTableView.estimatedRowHeight = 85;
    _arrayOfDates = [NSMutableDictionary dictionary];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// FSCalendarDelegate
- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date
{
    
    NSArray *days = [_arrayOfDates objectForKey:date];
    if(days!=nil){
        _scheduleArray = days;
        [_scheduleTableView reloadData];
    }
    else{
    _curDate = date;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    [_ActivityIndicator startAnimating];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    ScheduleFetch *scheduleFetch = [[ScheduleFetch alloc]initWithDelegate:self];
    [scheduleFetch getScheduleData:7:stringFromDate];
    }
    
}
- (void)didCompletedScheduleFetch:(id)daysArray{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [_arrayOfDates removeAllObjects];
    _daysArray = daysArray;
//   NSMutableArray *keys = [NSMutableArray array];
//    for(int j=0;j<[_daysArray count];j++){
//        Days *d = [_daysArray objectAtIndex:j];
//        [keys addObject:d.datetime];
//    }
//    NSArray *arraySortDate = [keys sortedArrayUsingComparator:
//                              ^(id obj1, id obj2) {
//                                  return [obj2 compare:obj1]; // note reversed comparison here
//                              }];
//    Days *days =[_daysArray objectAtIndex:1];
//    _scheduleArray = days.discs;
    for (int i=0; i<[daysArray count]; i++) {
        Days *day = [daysArray objectAtIndex:i];
        NSArray *scheduleForDay;
        NSDate *dateTemp = [dateFormatter dateFromString:day.datetime];
        scheduleForDay = day.discs;
        if ([scheduleForDay count]==0) {
            Schedule *schedule = [[Schedule alloc]init];
            schedule.UID_disc = @"Отсутствуют занятия на этот день";
            scheduleForDay = [NSArray arrayWithObject:schedule];
        }

        [_arrayOfDates setObject:scheduleForDay forKey:dateTemp];
    }
    NSArray *days = [_arrayOfDates objectForKey:_curDate];
    if(days!=nil){
        _scheduleArray = days;
        [_scheduleTableView reloadData];
    }

    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [_ActivityIndicator stopAnimating];
}

- (void)didCompleteWithError:(NSError*)error{
    int yPosition = 10;
    UILabel *label = [[UILabel alloc] initWithFrame: CGRectMake(0, yPosition, self.view.frame.size.width, 0)];
    [label setText: @"Отсутствуют данные о расписание"];
    [label setBackgroundColor: [UIColor clearColor]];
    [label setNumberOfLines: 0];
    [label sizeToFit];
    [label setCenter: CGPointMake(self.view.center.x, self.view.center.y)];
    [[self view] addSubview:label];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [_ActivityIndicator stopAnimating];
    
}
+ (BOOL) dateCheck:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate {
           return (([date compare:beginDate] != NSOrderedAscending) && ([date compare:endDate] != NSOrderedDescending));
       }

#pragma mark - TableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH-mm-ss"];
    NSArray *sortedArray = [_scheduleArray sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        Schedule *s =(Schedule*)a;
        Schedule *s1 =(Schedule*)b;
        NSDate *first = [dateFormatter dateFromString:s.startDate];
        NSDate *second = [dateFormatter dateFromString:s1.startDate];;
        return [first compare:second];
    }];
    Schedule *schedule = [sortedArray objectAtIndex:indexPath.section];
    ScheduleTableViewCell* cell =[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    //NSArray *values = [schedule.UID_disc allValues];
    cell.lessonNameLabel.text =schedule.UID_disc;
    NSDate *date = [dateFormatter dateFromString:schedule.startDate];
    NSDate *date1 = [dateFormatter dateFromString:schedule.endDate];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"HH:mm";
    if (![schedule.UID_disc isEqualToString:@"Отсутствуют занятия на этот день"]) {
        //cell.hoursLabel.text = [NSString stringWithFormat:@"%ld:%ld-%ld:%ld",(long)[components hour],(long)[components minute],(long)[components1 hour],(long)[components1 minute]];
        cell.hoursLabel.text = [NSString stringWithFormat:@"%@:%@",[formatter stringFromDate:date],[formatter stringFromDate:date1]];
    }
    cell.roomLabel.text = schedule.UID_room;
    cell.teacherLaber.text = schedule.UID_lecturer;
    cell.typeOfLessonLabel.text= schedule.UID_type;
    if ([schedule.status isEqualToString:@"c9e390e8-8e28-11e7-80d8-005056a02ba9"]) {
        cell.statusOfLessonLabel.text = @"Замена";
    }
    if ([schedule.status isEqualToString:@"c9e390ea-8e28-11e7-80d8-005056a02ba9"]) {
        cell.statusOfLessonLabel.text = @"Отмена";
    }
    
    return cell;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [_scheduleArray count];
}
-(CGFloat)tableView:(UITableView*)tableView
heightForHeaderInSection:(NSInteger)section {

    return 1.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
    
}
#pragma mark - <FSCalendarDelegate>
- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated
{
    self.calendarHeightConstraint.constant = 100;
    [self.view layoutIfNeeded];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
