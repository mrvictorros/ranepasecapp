//
//  ProfileTableViewCell.h
//  RanepaApp
//
//  Created by Росляков Виктор on 06.09.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *descLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

@end
