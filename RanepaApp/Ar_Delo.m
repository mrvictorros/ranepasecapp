//
//  Ar_Delo.m
//  RanepaApp
//
//  Created by Росляков Виктор on 02.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "Ar_Delo.h"

@implementation Ar_Delo

+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    id(^transformBlock1)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSNumber class]]) {
            return value;
        }
        return 0;
    };
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"Profil" toProperty:@"Profil" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Fakultet" toProperty:@"UID_Fakultet" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Filial" toProperty:@"UID_Filial" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Forma" toProperty:@"UID_Forma" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_GodPostupleniya" toProperty:@"UID_GodPostupleniya" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Gruppa" toProperty:@"UID_Gruppa" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Kafedra" toProperty:@"UID_Kafedra" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Napravlenie" toProperty:@"UID_Napravlenie" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Obuch" toProperty:@"UID_Obuch" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Osnova" toProperty:@"UID_Osnova" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Profil" toProperty:@"UID_Profil" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Fakultet" toProperty:@"Fakultet" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Filial" toProperty:@"Filial" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Forma" toProperty:@"Forma" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"GodPostupleniya" toProperty:@"GodPostupleniya" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Gruppa" toProperty:@"Gruppa" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Kafedra" toProperty:@"Kafedra" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Level" toProperty:@"Level" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Napravlenie" toProperty:@"Napravlenie" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Osnova" toProperty:@"Osnova" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Status" toProperty:@"Status" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Kurs" toProperty:@"Kurs" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Ar_UID_ScheduleGroups" toProperty:@"Ar_UID_ScheduleGroups" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Time" toProperty:@"Time" withValueBlock:transformBlock1];
        [mapping mapKeyPath:@"UID_Status" toProperty:@"UID_Status" withValueBlock:transformBlock1];
        [mapping mapKeyPath:@"UID_Level" toProperty:@"UID_Level" withValueBlock:transformBlock1];
        
 //       [mapping mapPropertiesFromArray:@[
        //                                  @"Kurs",
         //                                 @"Time",
//                                          @"UID_Fakultet",
//                                          @"UID_Filial",
//                                          @"UID_Forma",
//                                          @"UID_GodPostupleniya",
//                                          @"UID_Gruppa",
//                                          @"UID_Kafedra",
         //                                 @"UID_Level",
//                                          @"UID_Napravlenie",
//                                          @"UID_Obuch",
//                                          @"UID_Osnova",
//                                          @"UID_Profil",
          //                                @"UID_Status",
//                                          @"Fakultet",
//                                          @"Filial",
//                                          @"Forma",
//                                          @"GodPostupleniya",
//                                          @"Gruppa",
//                                          @"Kafedra",
//                                          @"Level",
//                                          @"Napravlenie",
//                                          @"Osnova",
//                                          @"Status"]];
    }];
    // return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) { [mapping mapPropertiesFromArray:@[@"Email"]];}];
}


@end
