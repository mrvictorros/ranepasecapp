//
//  HelpProtocol.h
//  RanepaApp
//
//  Created by Росляков Виктор on 23.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

@protocol HelpProtocol

- (void)didCompletedHelpFetch:(id)arrayOfArray;

- (void)didCompleteWithError:(NSError*)error;


@end