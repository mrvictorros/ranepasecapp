//
//  ProfileViewController.m
//  RanepaApp
//
//  Created by Росляков Виктор on 23.09.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "ProfileViewController.h"
#import "SWRevealViewController.h"
#import "ProfileHTTPCommunication.h"
#import "Api.h"
#import "Autorisation.h"
#import "Profile.h"
#import "Ar_Delo.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ProfileTableViewCell.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
@import Firebase;
@interface ProfileViewController ()

@property(nonatomic, strong)Profile* profile;
@property(nonatomic, strong)Ar_Delo* ar_Delo;
@property(nonatomic, strong)NSMutableArray* nameCell;
@property(nonatomic, strong)NSMutableArray* discCell;
-(void)setText:(NSString*)text:(UILabel*)label;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];//FB
    
    //NSLog(@"Subscribed to news topic");
    self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
    self.profileImage.clipsToBounds = YES;
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    [_activityIndicator startAnimating];
    _profileTableView.rowHeight = UITableViewAutomaticDimension;
    _profileTableView.estimatedRowHeight = 85;
    [self getDataProfile];
    self.nameCell = [[NSMutableArray alloc] initWithCapacity:3];
    self.discCell = [[NSMutableArray alloc] initWithCapacity:3];
//    Autorisation *autorisation = [[Autorisation alloc]init];
 //   [autorisation autorisation];
    UIFont *font = [UIFont fontWithName:@"helvetica-light" size:15.0];
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    UIColorFromRGB(0x6D1416),NSForegroundColorAttributeName,
                                    font,NSFontAttributeName,nil];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.title = @"Профиль";
    
}
- (void)didCompleteWithError:(NSError*)error{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Проблемы при загрузке данных."
                                                                   message:@"При отсутствии необходимых данных, напишите на tech-support@ranepa.ru"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];

    [_activityIndicator stopAnimating];
}
- (void)didCompletedProfileFetch:(id)profile :(id)arDelo{
    self.profile =profile;
    self.ar_Delo = arDelo;
    
    self.nameLabel.text = [NSString stringWithFormat:@"%@ %@",self.profile.Imya,self.profile.Familiya];
    self.groupLabel.text = self.ar_Delo.Gruppa;
    //self.filialLabel.text = self.ar_Delo.Filial;
    //self.facultyLabel.text = self.ar_Delo.Fakultet;
    self.formStudyLabel.text = self.ar_Delo.Forma;
   // self.profileLabel.text = self.ar_Delo.Profil;
    self.kursLabel.text = self.ar_Delo.Kurs;
    self.emailLabel.text = self.profile.Email;
    [self.nameCell insertObject:@"Филиал" atIndex:0];
    [self.nameCell insertObject:@"Факультет" atIndex:1];
    [self.nameCell insertObject:@"Профиль" atIndex:2];
    [self.discCell insertObject:self.ar_Delo.Filial atIndex:0];
    [self.discCell insertObject:self.ar_Delo.Fakultet atIndex:1];
    [self.discCell insertObject:self.ar_Delo.Profil atIndex:2];
    [_profileTableView reloadData];
    NSURL *url = [NSURL URLWithString:[self.profile.Photo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [self.profileImage sd_setImageWithURL:url];
    [self.profileImage setContentMode:UIViewContentModeScaleAspectFill];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *log = [userDefaults objectForKey:@"login"];
    [userDefaults setObject:[NSString stringWithFormat:@"%@ %@ %@",self.profile.Familiya,self.profile.Imya,self.profile.Otchestvo] forKey:@"profileFIO"];
    [userDefaults setObject:self.ar_Delo.Fakultet forKey:@"profileInstitute"];
    [userDefaults setObject:[NSString stringWithFormat:@"%@ (%@, %@)",self.ar_Delo.Level,self.ar_Delo.Napravlenie,self.ar_Delo.Profil] forKey:@"profileProgram"];
    [userDefaults setObject:self.ar_Delo.GodPostupleniya forKey:@"profileYear"];
    [userDefaults setObject:self.profile.LoginAD forKey:@"loginAD"];
    [userDefaults setObject:self.ar_Delo.UID_Obuch forKey:@"UID_Obuch"];
    [userDefaults setObject:self.ar_Delo.Ar_UID_ScheduleGroups forKey:@"Ar_UID_ScheduleGroups"];
    [userDefaults setObject:[NSString stringWithFormat:@"Login_AD=%@&UID_Obuch=%@&UID_Level=%@&UID_Status=%@&UID_Osnova=%@&key=%@",log,self.ar_Delo.UID_Obuch,self.ar_Delo.UID_Level,self.ar_Delo.UID_Status,self.ar_Delo.UID_Osnova,[userDefaults objectForKey:@"publicKey"]] forKey:@"secondUrl"];
    [_activityIndicator stopAnimating];
    [userDefaults synchronize];
    for (int i=0; i<[self.profile.fcm_topics count]; i++) {
        NSString* topic =[NSString stringWithFormat:@"%@%@",@"/topics/",[self.profile.fcm_topics objectAtIndex:i]];
        [[FIRMessaging messaging] subscribeToTopic:topic];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)getDataProfile{
    if(self.profile ==nil){
    ProfileHTTPCommunication *http = [[ProfileHTTPCommunication alloc]initWithDelegate:self];
    [http getProfileData];
    }
}
#pragma mark - TableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    ProfileTableViewCell* cell =[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.nameLabel.text = [self.discCell objectAtIndex:indexPath.section];
    cell.descLabel.text = [self.nameCell objectAtIndex:indexPath.section];
    
    return cell;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [_nameCell count];
}
-(CGFloat)tableView:(UITableView*)tableView
heightForHeaderInSection:(NSInteger)section {
    
    return 1.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
