//
//  ApiPostChange.h
//  RanepaApp
//
//  Created by Росляков Виктор on 16.03.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

@protocol ApiPostChange

- (void)didCompletedFetchPost;

- (void)didCompleteFetchPostWithError;

@end
