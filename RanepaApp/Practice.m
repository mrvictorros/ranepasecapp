//
//  Practice.m
//  RanepaApp
//
//  Created by Росляков Виктор on 10.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "Practice.h"

@implementation Practice
+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    id(^transformBlock1)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSNumber class]]) {
            return value;
        }
        return 0;
    };
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"Ar_demands" toProperty:@"Ar_demands" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Ar_files" toProperty:@"Ar_files" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"DateEnd" toProperty:@"DateEnd" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"nDateEnd" toProperty:@"nDateEnd" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"DateStart" toProperty:@"DateStart" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"nDateStart" toProperty:@"nDateStart" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Department" toProperty:@"Department" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Feedback" toProperty:@"Feedback" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Goal" toProperty:@"Goal" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"StudentPost" toProperty:@"StudentPost" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Tasks" toProperty:@"Tasks" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Company" toProperty:@"UID_Company" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Draft" toProperty:@"UID_Draft" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Obuch" toProperty:@"UID_Obuch" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Stage" toProperty:@"UID_Stage" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"UID_Type" toProperty:@"UID_Type" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Type" toProperty:@"Type" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"Update" toProperty:@"Update" withValueBlock:transformBlock];
        
        [mapping mapKeyPath:@"step" toProperty:@"step" withValueBlock:transformBlock1];
        
        [mapping hasOne:[Obj_CompanyInfo class] forKeyPath:@"Obj_CompanyInfo"];
        [mapping hasOne:[Obj_Tutor class] forKeyPath:@"Obj_Curator"];
        [mapping hasOne:[Obj_Director class] forKeyPath:@"Obj_Responsable"];
        
        [mapping mapPropertiesFromArray:@[@"ReadOnly"]];
        
    }];
}
@end
