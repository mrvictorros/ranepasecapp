//
//  ResetPassFetch.h
//  RanepaApp
//
//  Created by Росляков Виктор on 29.06.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResetPassProtocol.h"
#import "EasyMapping.h"

@interface ResetPassFetch : NSObject

-(id)initWithDelegate:(id)delegate;

- (void)doReset:(NSString *)login;

@property(nonatomic, weak)id <ResetPassProtocol> delegate;

@end
