//
//  AcademicPlanFetch.h
//  RanepaApp
//
//  Created by Росляков Виктор on 22.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AcademicPlanProtocol.h"

@interface AcademicPlanFetch : NSObject

- (void)getAcademicPlanData;

-(id)initWithDelegate:(id)delegate;

@property(nonatomic, weak)id <AcademicPlanProtocol> delegate;

@end
