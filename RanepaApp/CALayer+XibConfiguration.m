//
//  CALayer+XibConfiguration.m
//  RanepaApp
//
//  Created by Росляков Виктор on 22.12.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "CALayer+XibConfiguration.h"

@implementation CALayer(XibConfiguration)

-(void)setBorderUIColor:(UIColor*)color
{
    self.borderColor = color.CGColor;
}

-(UIColor*)borderUIColor
{
    return [UIColor colorWithCGColor:self.borderColor];
}

@end
