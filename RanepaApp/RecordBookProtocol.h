//
//  RecordBookProtocol.h
//  RanepaApp
//
//  Created by Росляков Виктор on 21.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

@protocol RecordBookProtocol

- (void)didCompletedRecordBookFetch:(id)arrayOfPeriods:(id)arrayOfRecordBook;

- (void)didCompleteWithError:(NSError*)error;


@end