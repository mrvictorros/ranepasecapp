//
//  Periods.m
//  RanepaApp
//
//  Created by Росляков Виктор on 30.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "Periodss.h"

@implementation Periodss

+(EKObjectMapping *)objectMapping
{
    id(^transformBlock)(NSString *key, id value) = ^id(NSString *key, id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return value;
        }
        return @"";
    };
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"s" toProperty:@"s" withValueBlock:transformBlock];
        [mapping mapKeyPath:@"y" toProperty:@"y" withValueBlock:transformBlock];
        
    }];
}
@end
