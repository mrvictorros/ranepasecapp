//
//  SidebarViewController.h
//  RanepaApp
//
//  Created by Росляков Виктор on 21.09.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SidebarViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
@property (weak, nonatomic) IBOutlet UIImageView *logoutImage;
- (IBAction)pressLogoutButton:(id)sender;
- (IBAction)pressEmail:(id)sender;

@end
