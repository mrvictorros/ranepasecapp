//
//  Obj_CompanyInfo.h
//  RanepaApp
//
//  Created by Росляков Виктор on 22.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Obj_Director.h"
#import "EasyMapping.h"


@interface Obj_CompanyInfo : NSObject <EKMappingProtocol>

@property(nonatomic, strong)NSString* Address;
@property(nonatomic, strong)NSString* BankInfo;
@property(nonatomic, strong)NSString* Name;
@property(nonatomic, strong)NSString* Type;

@property(nonatomic, strong)Obj_Director *Obj_Director;
@property(nonatomic, strong)Obj_Director *Obj_Responsable;

+(EKObjectMapping *)objectMapping;

@end
