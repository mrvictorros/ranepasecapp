//
//  Achievements.h
//  RanepaApp
//
//  Created by Росляков Виктор on 23.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"
#import "Obj_Document.h"


@interface Achievements : NSObject <EKMappingProtocol>

@property(strong,nonatomic)NSString* UID_Draft;
@property(strong,nonatomic)NSString* Dost;
@property(strong,nonatomic)NSString* UID_Dost;
@property(strong,nonatomic)NSString* Category;

@property(nonatomic, copy)NSNumber* Ball;

@property (nonatomic,readwrite) BOOL readonly;// булево, считается успешно сданным,

@property (nonatomic, strong) Obj_Document *Obj_Document;

+(EKObjectMapping *)objectMapping;

@end
