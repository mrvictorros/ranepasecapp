//
//  NameCellObject.h
//  RanepaApp
//
//  Created by Росляков Виктор on 31.01.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TableViewCellObjectProtocol.h"


@interface NameCellObject : NSObject <TableViewCellObjectProtocol>

@property(nonatomic, strong)NSString *titleName;
@property(nonatomic, strong)NSString *subTitle;

-(NSString *) typeOfCell;

+ (NSString *)identifierForReusableCell;

@end
