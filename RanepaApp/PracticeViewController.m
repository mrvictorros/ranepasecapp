//
//  PracticeViewController.m
//  RanepaApp
//
//  Created by Росляков Виктор on 06.10.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "PracticeViewController.h"
#import "SWRevealViewController.h"
#import "PracticeTableViewCell.h"
#import "PracticeDataService.h"
#import "Practice.h"
#import "PracticeDetailViewController.h"
#import "PracticeFetch.h"
#import "PracticeProtocol.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface PracticeViewController ()

@property(nonatomic,strong) PracticeDataService *practiceDataService;

@property(nonatomic,strong)NSArray* practiceArray;

@end

@implementation PracticeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
   // self.practiceDataService = [[PracticeDataService alloc] init];
    PracticeFetch *practiceFetch = [[PracticeFetch alloc]initWithDelegate:self];
    [practiceFetch getPracticeData];
    [self.activityIndicator startAnimating];
    UIFont *font = [UIFont fontWithName:@"helvetica-light" size:15.0];
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    UIColorFromRGB(0x6D1416),NSForegroundColorAttributeName,
                                    font,NSFontAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.title = @"Практика";
    _practiceTableView.rowHeight = UITableViewAutomaticDimension;
    _practiceTableView.estimatedRowHeight = 60;

}
- (void)didCompleteWithError:(NSError*)error{
    int yPosition = 10;
    UILabel *label = [[UILabel alloc] initWithFrame: CGRectMake(0, yPosition, self.view.frame.size.width, 0)];
    [label setText: @"Отстутствуют данные о практике"];
    [label setBackgroundColor: [UIColor clearColor]];
    [label setNumberOfLines: 0];
    [label sizeToFit];
    [self.activityIndicator stopAnimating];
    [label setCenter: CGPointMake(self.view.center.x, self.view.center.y)];
    [[self view] addSubview:label];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"ShowPracticeDetails"]) {
        NSIndexPath *indexPath = [self.practiceTableView indexPathForSelectedRow];
        Practice *practice = [self.practiceArray objectAtIndex:indexPath.section];
        [[segue destinationViewController] setDetailItem:practice];
//         DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
//         [controller setDetailItem:object];
//         controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
//         controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}
- (void)didCompletedPracticeFetch:(id)practiceArray{
    self.practiceArray = practiceArray;
    [self.practiceTableView reloadData];
    [_activityIndicator stopAnimating];

    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
    
}
-(CGFloat)tableView:(UITableView*)tableView
heightForHeaderInSection:(NSInteger)section {
//    if (section == 0) {
//        return 6.0;
//    }
    
    return 1.0;
}

//- (CGFloat)tableView:(UITableView*)tableView
//heightForFooterInSection:(NSInteger)section {
//    return 5.0;
//}
//
//- (UIView*)tableView:(UITableView*)tableView
//viewForHeaderInSection:(NSInteger)section {
//    return [[UIView alloc] initWithFrame:CGRectZero];
//}
//
//- (UIView*)tableView:(UITableView*)tableView
//viewForFooterInSection:(NSInteger)section {
//    return [[UIView alloc] initWithFrame:CGRectZero];
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PracticeTableViewCell* cell =[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    Practice *practice = self.practiceArray[indexPath.row];
    cell.positionLabel.text = practice.StudentPost;
    cell.companyLabel.text = practice.Obj_CompanyInfo.Name;
    cell.dateLabel.text = [NSString stringWithFormat:@"%@-%@",practice.nDateStart,practice.nDateEnd];
    
    return cell;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.practiceArray count];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
