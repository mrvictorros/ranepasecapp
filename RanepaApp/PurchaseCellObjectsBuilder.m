//
//  PurchaseCellObjectsBuilder.m
//  RanepaApp
//
//  Created by Росляков Виктор on 03.02.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import "PurchaseCellObjectsBuilder.h"
#import "NameCellObject.h"
#import "SelectCellObject.h"
#import "EditCellObject.h"
#import "Payer.h"
#import "Periods.h"


@implementation PurchaseCellObjectsBuilder

+ (NSArray *) buildCellObjectsWithPurchase:(Purchase *) purchase{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *FIO = [userDefaults objectForKey:@"profileFIO"];
 
    NSString *institute = [userDefaults objectForKey:@"profileInstitute"];
    NSString *program = [userDefaults objectForKey:@"profileProgram"];
    NSString *year = [userDefaults objectForKey:@"profileYear"];
    NameCellObject *firstObject = [[NameCellObject alloc ] init];
    firstObject.titleName = FIO;
    firstObject.subTitle = @"ФИО студента";
    SelectCellObject *secObject = [[SelectCellObject alloc ] init];
    secObject.titleArray = [NSMutableArray array];
    for(int i=0;i<[purchase.PAYER count];i++){
        Payer *payer = [purchase.PAYER objectAtIndex:i];//TODO
        if(payer.CanPay){
        [secObject.titleArray addObject:payer.Name];
        secObject.selectedIndex = i;
        }
    }
    secObject.subTitleSelectEdit = @"ФИО плательщика";
    NameCellObject * thirdObject = [[NameCellObject alloc ] init];
    thirdObject.titleName = year;
    thirdObject.subTitle = @"Год поступления";
    NameCellObject *fourthObject = [[NameCellObject alloc ] init];
    fourthObject.titleName = institute;
    fourthObject.subTitle = @"Институт/Факультет";
    NameCellObject *fifthObject = [[NameCellObject alloc ] init];
    fifthObject.titleName = program;
    fifthObject.subTitle = @"Программа";
    NameCellObject *sixthObject = [[NameCellObject alloc ] init];
    sixthObject.titleName = purchase.CONTRACT;
    sixthObject.subTitle = @"Номер договора";
    SelectCellObject *seventhObject = [[SelectCellObject alloc ] init];
    seventhObject.titleArray = [NSMutableArray array];
    for(int i=0;i<[purchase.truePeriods count];i++){
        NSArray *periods = [purchase.truePeriods objectAtIndex:i];
        [seventhObject.titleArray addObject:[NSString stringWithFormat:@"%@%@%@",[periods objectAtIndex:0],@" - ",[periods objectAtIndex:1]]];
        seventhObject.selectedIndex = i;
    }
    seventhObject.subTitleSelectEdit = @"Период";
    EditCellObject *eightObject = [[EditCellObject alloc]init];
    eightObject.subTitleEdit = @"Сумма платежа";
    NSArray *cellObjects  = [NSArray arrayWithObjects:firstObject,secObject,thirdObject,fourthObject,fifthObject,sixthObject,seventhObject,eightObject,nil];
    return cellObjects;
}

@end
