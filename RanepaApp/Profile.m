//
//  Profile.m
//  RanepaApp
//
//  Created by Росляков Виктор on 01.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//

#import "Profile.h"
#import "Ar_Delo.h"

@implementation Profile


+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[
                                          @"DataRojdeniya",
                                          @"Email",
                                          @"Familiya",
                                          @"Imya",
                                          @"LoginAD",
                                          @"Name",
                                          @"Otchestvo",
                                          @"Phone",
                                          @"Photo",
                                          @"Pol",
                                          @"Surname",
                                          @"fcm_topics"]];
        [mapping hasMany:[Ar_Delo class] forKeyPath:@"Ar_Delo"];
    }];
   // return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) { [mapping mapPropertiesFromArray:@[@"Email"]];}];
}

@end
