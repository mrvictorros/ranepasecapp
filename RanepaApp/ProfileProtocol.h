//
//  ProfileProtocol.h
//  RanepaApp
//
//  Created by Росляков Виктор on 02.11.16.
//  Copyright © 2016 RANEPA. All rights reserved.
//
#import "Profile.h"
@protocol ProfileProtocol

- (void)didCompletedProfileFetch:(id)profile:(id)arDelo;

- (void)didCompleteWithError:(NSError*)error;

@end
