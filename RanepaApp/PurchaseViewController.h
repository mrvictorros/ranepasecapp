//
//  PurchaseViewController.h
//  RanepaApp
//
//  Created by Росляков Виктор on 31.01.17.
//  Copyright © 2017 RANEPA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentTableDelegate.h"
#import "Purchase.h"

@interface PurchaseViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *purchaseTableView;

@property (strong, nonatomic) PaymentTableDelegate *tableDelegate;

@property (strong, nonatomic) Purchase *purchase;

@end
