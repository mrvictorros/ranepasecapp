//
//  Reg+CoreDataProperties.m
//  
//
//  Created by Росляков Виктор on 14.10.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Reg+CoreDataProperties.h"

@implementation Reg (CoreDataProperties)

@dynamic appcode;
@dynamic simcode;
@dynamic login;
@dynamic password;
@dynamic regestrated;
@dynamic publiccode;
@dynamic privatecode;

@end
