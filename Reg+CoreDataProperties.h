//
//  Reg+CoreDataProperties.h
//  
//
//  Created by Росляков Виктор on 14.10.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Reg.h"

NS_ASSUME_NONNULL_BEGIN

@interface Reg (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *appcode;
@property (nullable, nonatomic, retain) NSString *simcode;
@property (nullable, nonatomic, retain) NSString *login;
@property (nullable, nonatomic, retain) NSString *password;
@property (nullable, nonatomic, retain) NSNumber *regestrated;
@property (nullable, nonatomic, retain) NSString *publiccode;
@property (nullable, nonatomic, retain) NSString *privatecode;

@end

NS_ASSUME_NONNULL_END
